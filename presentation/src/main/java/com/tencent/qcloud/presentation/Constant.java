package com.tencent.qcloud.presentation;

/**
 * 常量
 */
public class Constant {

    public static final int ACCOUNT_TYPE = 7225;
    //sdk appid 由腾讯分配
    public static final int SDK_APPID = 1400014188;

}
