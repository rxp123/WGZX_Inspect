package com.zzhrtech.wgzx_inspect.ui.me;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;

public class MyInfoActivity extends BaseActivity {

    private TextView et_name,et_phone;
    private ImageView civ_head;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_info);
        toolbar.setNavigationIcon(R.mipmap.ic_close);
//        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                if (item.getTitle().equals("编辑")){
//                    setenable();
//                    item.setTitle("完成");
//                }else {
//                    disenable();
//                    item.setTitle("编辑");
//                }
//                return false;
//            }
//        });

        initView();

    }

    private void initView(){
        et_name = (TextView) findViewById(R.id.et_name);
//        et_nickname = (EditText) findViewById(R.id.nickname);
        et_phone = (TextView) findViewById(R.id.et_phone);
        civ_head = (ImageView) findViewById(R.id.civ_head);

        Picasso.with(this)
                .load(getCache().getAsString(keyHead))
                .placeholder(R.mipmap.ic_placeholde)
                .into(civ_head);
        et_name.setText(getCache().getAsString(keyNickname));
        et_phone.setText(getCache().getAsString(keyUsername));
    }

    private void disenable(){
        et_name.setEnabled(false);
        et_phone.setEnabled(false);
    }

    private void setenable(){
        et_name.setEnabled(true);
        et_phone.setEnabled(true);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_info, menu);
//        return true;
//    }
}
