package com.zzhrtech.wgzx_inspect.ui.task;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.media.Image;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.TextureMapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.bumptech.glide.Glide;
import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.Type;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.tencent.qcloud.ui.VideoInputDialog;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.disease.AreaListBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateContentBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateInfoBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateListBean;
import com.zzhrtech.wgzx_inspect.beans.disease.RoleListBean;
import com.zzhrtech.wgzx_inspect.beans.disease.ZhiPaiBean;
import com.zzhrtech.wgzx_inspect.beans.task.BaseBean;
import com.zzhrtech.wgzx_inspect.beans.task.BossListBean;
import com.zzhrtech.wgzx_inspect.beans.task.FkArceBean;
import com.zzhrtech.wgzx_inspect.beans.task.PhoneListBean;
import com.zzhrtech.wgzx_inspect.beans.task.StatusBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskDetailBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskFilterBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskReasonBean;
import com.zzhrtech.wgzx_inspect.ui.contact.VideoActivity;
import com.zzhrtech.wgzx_inspect.ui.disease.DiseaseMapActivity;
import com.zzhrtech.wgzx_inspect.ui.disease.DiseaseUploadActivity;
import com.zzhrtech.wgzx_inspect.ui.disease.ImagePickActivity;
import com.zzhrtech.wgzx_inspect.ui.me.MonitorActivity;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;
import com.zzhrtech.wgzx_inspect.utils.OkHttpPicManager;

import org.angmarch.views.NiceSpinner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.iwf.photopicker.PhotoPicker;

public class TaskDetailActivity extends BaseActivity implements GeocodeSearch.OnGeocodeSearchListener, OnDateSetListener {

    private ViewGroup photoGroup;
    Button btn_sure,btn_cancel,btn_turnsend,btn_needsp,btn_needupsp,btn_zhipai,btn_fenpai;
    TextureMapView mMapView;
    AMap aMap;

    private TextView tv_type,tv_kind,tv_content,
                tv_address,tv_time,tv_name,tv_phone,tv_note,tv_quyu;
    private String taskid,mstatus,mtabtitle;

    List<RoleListBean.Data> roleList;
    List<CateListBean.Data> cateList;
    List<CateInfoBean.Data> cateInfoList;
    List<CateContentBean.Data> cateContentList;
    List<BossListBean.Data> bossList;
    List<PhoneListBean.Data> danweiList;
    String mroleId = "",mcatId = "",mciId = "",mcisId = "",mbId = "",maId = "",mXiucontent = "",mXiutime = "",mBossId = "";
    String turnRoleId = "";
    String mbPerson = "",mbPhone = "",mbname = "";
    NiceSpinner spinner_dep,spinner_type,spinner_class,spinner_info,spinner_cancle,spinner_boss,spinner_shigong;
    EditText et_reason,et_otherReason,et_dwname,et_dwpeople,et_dwphone;
    LinearLayout layout_dwname,layout_dwpeople,layout_dwphone;
    EditText et_time;
    TextView tv_rizhi,tv_bname,tv_bperson,tv_bphone,tv_qxreason,tv_fkneirong;
    private TimePickerDialog timePickerDialog;
    private CheckBox box_tfxq,box_aqyh;
    private CardView card_box,card_fankui,card_qxreason;
    private ArrayList<String> thImgList = new ArrayList<String>();
    private String mapPoint;
    private List<TaskReasonBean.Data> ressonList;
    private String cancleReason = "";

    private final OkHttpClient client = new OkHttpClient();
    private ViewGroup mSelectedImagesContainer,layout_fankui,layout_quxiao;
    private ArrayList<String> fankuiImgList = new ArrayList<String>();

    private ArrayList<String> quImgList = new ArrayList<String>();

    private int picCount = 3;

    private String mVideo,mUppi;
//    private boolean paiOrsure; //true sure,false pai
    private String whereQuyu = "";//判断指派，转盘，分派的区域
    private int dialogIndex;
    private ArrayList<String> totalImages = new ArrayList<String>();

    CardView bCardview;
    MaterialDialog progress;
    private JSONArray arceArray;
    private String clickIt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        mMapView = (TextureMapView) findViewById(R.id.map);
        //在activity执行onCreate时执行mMapView.onCreate(savedInstanceState)，实现地图生命周期管理
        mMapView.onCreate(savedInstanceState);
        toolbar.setNavigationIcon(R.mipmap.ic_close);


        taskid = getIntent().getStringExtra("taskid");
        mstatus = getIntent().getStringExtra("status");
        mtabtitle = getIntent().getStringExtra("tabtitle");

        initMap();

        initView();


        timePickerDialog = new TimePickerDialog.Builder()
                .setType(Type.ALL)
                .setTitleStringId("时间选择")
                .setThemeColor(getResources().getColor(R.color.colorPrimary))
                .setCallBack(this)
                .build();

        httpGetTask(taskid);

        Log.d("id==========",taskid);

    }



    private void initView() {
        photoGroup = (ViewGroup) findViewById(R.id.selected_photos_container);
        btn_sure = (Button) findViewById(R.id.btn_sure);

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_turnsend = (Button) findViewById(R.id.btn_turnsend);
        btn_needsp = (Button) findViewById(R.id.btn_needsp);
        tv_quyu = (TextView) findViewById(R.id.tv_quyu);
        box_tfxq = (CheckBox) findViewById(R.id.box_qxgl);
        box_aqyh = (CheckBox) findViewById(R.id.box_aqyh);
        card_box = (CardView) findViewById(R.id.card_box);
        card_fankui = (CardView) findViewById(R.id.card_fankui);
        tv_rizhi = (TextView) findViewById(R.id.tv_rizhi);
        bCardview = (CardView) findViewById(R.id.bCardview);
        btn_needupsp = (Button) findViewById(R.id.btn_needupsp);
        btn_zhipai = (Button) findViewById(R.id.btn_zhipai);
        layout_fankui = (LinearLayout) findViewById(R.id.layout_fankui);
        card_qxreason = (CardView) findViewById(R.id.card_qxreason);
        tv_qxreason = (TextView) findViewById(R.id.tv_qxreason);
        tv_fkneirong = (TextView) findViewById(R.id.tv_fkneirong);
        btn_fenpai = (Button) findViewById(R.id.btn_fenpai);
        layout_quxiao = (LinearLayout) findViewById(R.id.layout_quxiao);

        tv_rizhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskDetailActivity.this,TaskRizhiActivity.class);
                intent.putExtra("taskid",taskid);
                startActivity(intent);
            }
        });


        btn_zhipai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whereQuyu = "zhipai";
                httpGetArea(mroleId);
            }
        });


        switch (mstatus){
            case "0":
                if (mtabtitle.equals("已发布")){
                    btn_sure.setText("删除");
                }else {
                    btn_sure.setText("确定");
                    //取消
                    btn_cancel.setVisibility(View.VISIBLE);
                    //非市民显示重大安全
                    card_box.setVisibility(View.VISIBLE);
                    //判断是否有指派权限
                    httpCanPai(getCache().getAsString(keyUid));
                    //判断是否有转派权限
                    httpCanZhuan(getCache().getAsString(keyUid));
                }
                break;
            case "1":
                btn_sure.setText("派修");
                btn_cancel.setVisibility(View.VISIBLE);

                //判断是否有指派权限
                httpCanPai(getCache().getAsString(keyUid));
                //判断是否有分派权限
                httpCanFen(getCache().getAsString(keyUid));
                break;
            case "2":
                card_qxreason.setVisibility(View.VISIBLE);
//                btn_sure.setVisibility(View.GONE);
                btn_sure.setText("恢复");

                //获取任务日志，获取取消原因
                httpGetRizhi(taskid);
                break;
            case "3":
                btn_sure.setVisibility(View.GONE);
                btn_sure.setText("确定");
                break;
            case "4":
                //取消
                btn_cancel.setVisibility(View.VISIBLE);
                btn_sure.setText("反馈");
                break;
            case "5":
                btn_sure.setVisibility(View.GONE);
                btn_sure.setText("确定");
                break;
            case "6":
                btn_sure.setText("审批");
                break;
        }
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                clickIt = "sure";
                //检查状态是否改变
                httpGetStatus(taskid);

            }
        });

        btn_needsp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                httpTaskPost(taskid,"6");

                clickIt = "needsp";
                httpGetStatus(taskid);

            }
        });

        btn_needupsp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickIt = "needsp";
                httpGetStatus(taskid);

//                AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
//                View view = getLayoutInflater().inflate(R.layout.dialog_needsp,null);
//
//                spinner_boss = (NiceSpinner) view.findViewById(R.id.spinner_boss);
//                spinner_boss.setOnItemSelectedListener(spinnerListener);
//                final EditText et_bossreason = (EditText) view.findViewById(R.id.et_reason);
//
//                httpGetBoss();
//
//                builder.setView(view);
//                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        httpUpTaskShenpi(mBossId,et_bossreason.getText().toString());
//                    }
//                });
//                builder.setNegativeButton("取消", null);
//
//                //设置对话框是可取消的
//                builder.setCancelable(true);
//
//                AlertDialog dialog = builder.create();
//
//                dialog.show();
            }
        });

        btn_turnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickIt = "turnsend";
                httpGetStatus(taskid);
            }
        });


        btn_fenpai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickIt = "fenpai";
                httpGetStatus(taskid);

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickIt = "cancel";
                httpGetStatus(taskid);
//
            }
        });

        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_kind = (TextView) findViewById(R.id.tv_kind);
        tv_content = (TextView) findViewById(R.id.tv_content);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_note = (TextView) findViewById(R.id.tv_note);
        tv_bname = (TextView) findViewById(R.id.tv_bname);
        tv_bperson = (TextView) findViewById(R.id.tv_bperson);
        tv_bphone = (TextView) findViewById(R.id.tv_bphone);

    }


    private void cancleClick(){
        AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_cancle,null);

        spinner_cancle = (NiceSpinner) view.findViewById(R.id.spinner_cancle);
        spinner_cancle.setOnItemSelectedListener(spinnerListener);
        et_otherReason = (EditText) view.findViewById(R.id.et_otherReason);

        mSelectedImagesContainer = (ViewGroup) view.findViewById(R.id.selected_photos_container);
        initCamera();

        httpGetReason();
        builder.setView(view);

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (et_otherReason.getText().toString().equals("")){
                    showToast("请填写原因");
                }else {
                    cancleReason = et_otherReason.getText().toString().trim();
                    httpTaskPost(taskid,"2");
                }

            }
        });
        builder.setNegativeButton("取消", null);


        //设置对话框是可取消的
        builder.setCancelable(true);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void needspClick(){
        AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_needsp,null);

        spinner_boss = (NiceSpinner) view.findViewById(R.id.spinner_boss);
        spinner_boss.setOnItemSelectedListener(spinnerListener);
        final EditText et_bossreason = (EditText) view.findViewById(R.id.et_reason);

        httpGetBoss();

        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                httpUpTaskShenpi(mBossId,et_bossreason.getText().toString());
            }
        });
        builder.setNegativeButton("取消", null);

        //设置对话框是可取消的
        builder.setCancelable(true);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    //确定按钮事件
    private void sureClick(){
        whereQuyu = "queding";

        switch (mstatus){
            case "0":
                if (mtabtitle.equals("已发布")){
                    new MaterialDialog.Builder(TaskDetailActivity.this)
                            .content("是否确认删除该任务？")
                            .negativeText("取消")
                            .positiveText("确定")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    httpDeletTask();
                                }
                            })
                            .show();

                }else {
                    //选择区域
                    httpGetArea(mroleId);
                }

                break;
            case "1":


                AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
                View view = getLayoutInflater().inflate(R.layout.dialog_shigong,null);

                spinner_shigong = (NiceSpinner) view.findViewById(R.id.spinner_shigong);
                spinner_shigong.setOnItemSelectedListener(spinnerListener);


                et_dwname = (EditText) view.findViewById(R.id.et_dwname);
                et_dwpeople = (EditText) view.findViewById(R.id.et_dwpeople);
                et_dwphone = (EditText) view.findViewById(R.id.et_dwphone);

                layout_dwname = (LinearLayout) view.findViewById(R.id.layout_dwname);
                layout_dwpeople = (LinearLayout) view.findViewById(R.id.layout_dwpeople);
                layout_dwphone = (LinearLayout) view.findViewById(R.id.layout_dwphone);

                httpGetBuilder();
                builder.setView(view);

                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mbId = "-1";
                        mbPerson = et_dwpeople.getText().toString();
                        mbPhone = et_dwphone.getText().toString();
//                                mbname = danweiList.get(which).getB_name();
//                                if (mbname.equals(""))
                        mbname = et_dwname.getText().toString();

                        httpTaskPost(taskid,"4");

                    }
                });
                builder.setNegativeButton("取消", null);


                //设置对话框是可取消的
                builder.setCancelable(true);

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case "2":

                new MaterialDialog.Builder(TaskDetailActivity.this)
                        .content("是否确认恢复该任务？")
                        .negativeText("取消")
                        .positiveText("确定")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                httpHuifu(taskid);
                            }
                        })
                        .show();

                break;
            case "3":
                showToast("-----");
                break;
            case "4":
                builder=new AlertDialog.Builder(TaskDetailActivity.this);
                view = getLayoutInflater().inflate(R.layout.dialog_feedback,null);

//                        ImageView iv_add = (ImageView) view.findViewById(R.id.iv_add);
                LinearLayout areaageLayout = (LinearLayout) view.findViewById(R.id.arecageLayout);
                arceArray = new JSONArray();

                initarceAddView(areaageLayout);

                mSelectedImagesContainer = (ViewGroup) view.findViewById(R.id.selected_photos_container);
                final EditText editText = (EditText) view.findViewById(R.id.et_content);
                initCamera();

                et_time = (EditText) view.findViewById(R.id.et_time);
                if (et_time != null)
                    et_time.setKeyListener(null);
                et_time.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timePickerDialog.show(getSupportFragmentManager(),"all");
                    }
                });

                builder.setView(view);
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mXiutime.isEmpty()){
                            showToast("请选择时间");
                        }else if (editText.getText().equals("")){
                            showToast("请填写内容");
                        }else {
                            mXiucontent = editText.getText().toString();
                            httpTaskPost(taskid, "5");
                        }
                    }
                });
                builder.setNegativeButton("取消",null);

                //设置对话框是可取消的
                builder.setCancelable(true);

                dialog = builder.create();

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        totalImages.clear();
                    }
                });
                dialog.show();
//
                break;
            case "5":
                showToast("-----");
                break;
            case "6":

                builder=new AlertDialog.Builder(TaskDetailActivity.this);
                view = getLayoutInflater().inflate(R.layout.dialog_shenpi,null);

                final RadioButton radio_pass = (RadioButton) view.findViewById(R.id.radio_pass);
                final RadioButton radio_no = (RadioButton) view.findViewById(R.id.radio_no);
                final EditText shenpi_reason = (EditText) view.findViewById(R.id.et_reason);

                builder.setView(view);
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (radio_pass.isChecked()){
                            httpTaskShenpi(taskid,"1",shenpi_reason.getText().toString());
                        }else if (radio_no.isChecked()){
                            httpTaskShenpi(taskid,"2",shenpi_reason.getText().toString());
                        }
                    }
                });
                builder.setNegativeButton("取消", null);

                //设置对话框是可取消的
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();

                break;
        }
    }

    private void dialogFenpai(){
        AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_turnsend,null);
        spinner_dep = (NiceSpinner) view.findViewById(R.id.spinner_dep);
        spinner_dep.setOnItemSelectedListener(spinnerListener);
        spinner_type = (NiceSpinner) view.findViewById(R.id.spinner_type);
        spinner_type.setOnItemSelectedListener(spinnerListener);
        spinner_class = (NiceSpinner) view.findViewById(R.id.spinner_class);
        spinner_class.setOnItemSelectedListener(spinnerListener);
        spinner_info = (NiceSpinner) view.findViewById(R.id.spinner_info);
        spinner_info.setOnItemSelectedListener(spinnerListener);
        et_reason = (EditText) view.findViewById(R.id.et_reason);
        httpGetRole();
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                httpTaskFen(taskid);
            }
        });
        builder.setNegativeButton("取消", null);

        //设置对话框是可取消的
        builder.setCancelable(true);

        AlertDialog dialog = builder.create();

        dialog.show();
    }


    private void dialogZhuanpai(){
        AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_turnsend,null);
        spinner_dep = (NiceSpinner) view.findViewById(R.id.spinner_dep);
        spinner_dep.setOnItemSelectedListener(spinnerListener);
        spinner_type = (NiceSpinner) view.findViewById(R.id.spinner_type);
        spinner_type.setOnItemSelectedListener(spinnerListener);
        spinner_class = (NiceSpinner) view.findViewById(R.id.spinner_class);
        spinner_class.setOnItemSelectedListener(spinnerListener);
        spinner_info = (NiceSpinner) view.findViewById(R.id.spinner_info);
        spinner_info.setOnItemSelectedListener(spinnerListener);
        et_reason = (EditText) view.findViewById(R.id.et_reason);
        httpGetRole();
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                httpTaskTurn(taskid);
            }
        });
        builder.setNegativeButton("取消", null);

        //设置对话框是可取消的
        builder.setCancelable(true);

        AlertDialog dialog = builder.create();

        dialog.show();
    }


    int positions = 0;
    private void initarceAddView(final LinearLayout areaageLayout){
        final View fkView = LayoutInflater.from(TaskDetailActivity.this).inflate(R.layout.item_dialogarce, null);
        final ImageView iv_add = (ImageView) fkView.findViewById(R.id.iv_add);
        final TextView tvType = (TextView) fkView.findViewById(R.id.tv_type);
        final TextView tvClass = (TextView) fkView.findViewById(R.id.tv_class);
        final TextView tvContent = (TextView) fkView.findViewById(R.id.tv_content);
        final TextView tvArce = (TextView) fkView.findViewById(R.id.tv_arce);
        final TextView tvDanwei = (TextView) fkView.findViewById(R.id.tv_danwei);
        final boolean[] picIsAdd = {true};
        iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picIsAdd[0]) {
                    new TaskAddDialog(TaskDetailActivity.this, mroleId, new TaskAddDialog.OndataListener() {
                        @Override
                        public void onData(String typestr, String typeid, String classstr, String classid, String contentstr, String contentid, String arec, String unitstr, String unitid) {
                            tvType.setText(typestr);
                            tvClass.setText(classstr);
                            tvContent.setText(contentstr);
                            tvArce.setText(arec);
                            tvDanwei.setText(unitstr);

                            JSONObject object = new JSONObject();
                            try {
                                object.put("task_id", taskid);
                                object.put("cat_id", typeid);
                                object.put("ci_id", classid);
                                object.put("cis_id", contentid);
                                object.put("unit_id", unitid);
                                object.put("val", arec);
                                arceArray.put(object);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            picIsAdd[0] = false;
                            areaageLayout.setTag(positions);


                            positions++;
                            iv_add.setImageResource(R.mipmap.ic_arceagejian);
                            initarceAddView(areaageLayout);
                        }
                    }).show();

                }else {
                    areaageLayout.removeView(fkView);
                    arceArray.remove((int)areaageLayout.getTag());

                }
            }
        });
        areaageLayout.addView(fkView);
    }


    //初始化照相按钮
    private void initCamera(){
        View imageHolder = LayoutInflater.from(this).inflate(R.layout.item_camera, null);
        ImageView imageView = (ImageView) imageHolder.findViewById(R.id.media_image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPhotoPermission();
            }
        });
        mSelectedImagesContainer.addView(imageHolder);
    }


    private void checkPhotoPermission() {

        int readstorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        boolean readStoragePermissionGranted = readstorage != PackageManager.PERMISSION_GRANTED;
        boolean cameraPermissionGranted = camera != PackageManager.PERMISSION_GRANTED;

        if (readStoragePermissionGranted || cameraPermissionGranted) {

            // Should we show an explanation?
            String[] permissions;
            if (readStoragePermissionGranted && cameraPermissionGranted) {
                permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA };
            } else {
                permissions = new String[] {
                        readStoragePermissionGranted ? Manifest.permission.READ_EXTERNAL_STORAGE
                                : Manifest.permission.CAMERA
                };
            }
            ActivityCompat.requestPermissions(this,
                    permissions,
                    124);

        } else {
            // Permission granted
            PhotoPicker.builder()
                    .setPhotoCount(picCount - totalImages.size())
                    .setShowCamera(true)
                    .setShowGif(true)
                    .setPreviewEnabled(false)
                    .start(TaskDetailActivity.this, PhotoPicker.REQUEST_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == 124){
                PhotoPicker.builder()
                        .setPhotoCount(picCount - totalImages.size())
                        .setShowCamera(true)
                        .setShowGif(true)
                        .setPreviewEnabled(false)
                        .start(TaskDetailActivity.this, PhotoPicker.REQUEST_CODE);
            }

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //反馈图片 取消图片
    private void showFankui(final ArrayList<String> imageList,ViewGroup layout) {
        //dip转换为sp
        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());

        for (int i = 0;i < imageList.size();i++) {
            View imageHolder = LayoutInflater.from(this).inflate(R.layout.item_image, null);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);
            final int currentPage = i;
            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(TaskDetailActivity.this,ImagePickActivity.class);
                    intent.putStringArrayListExtra("images",imageList);
                    intent.putExtra("current",currentPage);
                    intent.putExtra("channel","task");
                    startActivity(intent);
                }
            });

            Picasso.with(this)
                    .load(imageList.get(i))
                    .placeholder(R.mipmap.ic_placeholde)
                    .into(thumbnail);

            layout.addView(imageHolder);
            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));
        }
    }

    private void showMedia(final List<String> images) {
        // Remove all views before
        // adding the new ones.
        mSelectedImagesContainer.removeAllViews();

        if (images.size() >= 1) {
            mSelectedImagesContainer.setVisibility(View.VISIBLE);
        }

        //dip转换为sp
        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 72, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 72, getResources().getDisplayMetrics());

        for (int i = 0; i < images.size();i++) {
            View imageHolder = LayoutInflater.from(this).inflate(R.layout.item_image, null);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);

            final int currentPage = i;
            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TaskDetailActivity.this,ImagePickActivity.class);
                    intent.putStringArrayListExtra("images",totalImages);
                    intent.putExtra("current",currentPage);
                    intent.putExtra("channel","upload");
                    startActivityForResult(intent,3);

                }
            });

            Glide.with(this)
                    .load(images.get(i))
                    .fitCenter()
                    .into(thumbnail);

            mSelectedImagesContainer.addView(imageHolder);
            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));

        }

        if (images.size() < 3) {
            initCamera();
        }

    }


    private void showPic(final ArrayList<String> imageList) {
        //dip转换为sp
        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());

        for (int i = 0;i < imageList.size();i++) {
            View imageHolder = LayoutInflater.from(this).inflate(R.layout.item_image, null);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);
            final int currentPage = i;
            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(TaskDetailActivity.this,ImagePickActivity.class);
                    intent.putStringArrayListExtra("images",imageList);
                    intent.putExtra("current",currentPage);
                    intent.putExtra("channel","task");
                    startActivity(intent);
                }
            });

            Picasso.with(this)
                    .load(imageList.get(i))
                    .placeholder(R.mipmap.ic_placeholde)
                    .into(thumbnail);

            photoGroup.addView(imageHolder);
            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));
        }
    }

    /**
     * 初始化AMap对象
     */
    private void initMap() {
        if (aMap == null) {
            aMap = mMapView.getMap();

            aMap.setOnMapClickListener(new AMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    Intent intent = new Intent(TaskDetailActivity.this,TaskMapActivity.class);
                    intent.putExtra("point",mapPoint);
                    startActivity(intent);
                }
            });

            //更改可视区域

//            setUpMap();
        }
    }

    @Override
    public void onDateSet(TimePickerDialog timePickerView, long millseconds) {
        mXiutime = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(millseconds));

        Log.d("xiutime=======",mXiutime);
        et_time.setText(DateFormat.getDateTimeInstance().format(new Date(millseconds)));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，实现地图生命周期管理
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {

    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == 0){

                progress.dismiss();

                TaskDetailBean.Data data = (TaskDetailBean.Data) msg.obj;

                tv_type.setText(data.getCat_name());
                tv_kind.setText(data.getCi_name());
                tv_content.setText(data.getCis_name());
                tv_address.setText(data.getTask_address());
                tv_time.setText(data.getTask_date());
                tv_name.setText(data.getTask_name());
                tv_phone.setText(data.getTask_phone());
                tv_note.setText(data.getTask_detail());

                if (data.getB_name() != null) {
                    bCardview.setVisibility(View.VISIBLE);
                    tv_bname.setText(data.getB_name());
                    tv_bperson.setText(data.getB_person());
                    tv_bphone.setText(data.getB_phone());
                }

                if (data.getArea() != null)
                tv_quyu.setText(data.getArea().getA_name());

                if (mtabtitle.equals("待确认") & data.getPi().equals("1")){
                    btn_needsp.setVisibility(View.VISIBLE);
                }

                if (mtabtitle.equals("已发布") && btn_sure.getText().equals("删除") && !data.getStatus().equals("0")){
                    btn_sure.setVisibility(View.GONE);
                }

                mVideo = data.getVideo();
                mapPoint = data.getTask_point();

                mUppi = data.getUppi();
                if (mstatus.equals("6")){
                    if (mUppi.equals(getCache().getAsString(keyUid))){
                        btn_needupsp.setVisibility(View.VISIBLE);
                    }else {
                        btn_needupsp.setVisibility(View.GONE);
                    }
                }


                String[] lats = data.getTask_point().split(",");
                LatLng latLng = new LatLng(Double.parseDouble(lats[1]),Double.parseDouble(lats[0]));

                CameraUpdate update = CameraUpdateFactory.newCameraPosition(new CameraPosition(
                        latLng, 17, 30, 0));
                aMap.moveCamera(update);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                aMap.addMarker(markerOptions);

                if (data.getXiuOneImg() != null){
                    fankuiImgList.add(data.getXiuOneImg());
                    if (data.getXiuTwoImg() != null){
                        fankuiImgList.add(data.getXiuTwoImg());
                        if (data.getXiuThreeImg() != null){
                            fankuiImgList.add(data.getXiuThreeImg());
                        }
                    }
                }

                if (data.getFanneirong() != null) {
                    if (!data.getFanneirong().equals("") || fankuiImgList.size() > 0) {
                        card_fankui.setVisibility(View.VISIBLE);
                        tv_fkneirong.setText(data.getFanneirong());
                    }
                }

                if (fankuiImgList.size() > 0) {
                    showFankui(fankuiImgList,layout_fankui);
                }

                if (data.getQuOneImg() != null){
                    quImgList.add(data.getQuOneImg());
                    if (data.getQuTwoImg() != null){
                        quImgList.add(data.getQuTwoImg());
                        if (data.getQuThreeImg() != null){
                            quImgList.add(data.getQuThreeImg());
                        }
                    }
                }

                if (quImgList.size() > 0){
                    showFankui(quImgList,layout_quxiao);
                }

                if (data.getOneImgTh() != null){
                    thImgList.add(data.getOneImg());
                    if (data.getTwoImgTh() != null){
                        thImgList.add(data.getTwoImg());
                        if (data.getThreeImgTh() != null){
                            thImgList.add(data.getThreeImg());
                        }
                    }
                }

                showPic(thImgList);

                mroleId = data.getDep_id();
            }else if (msg.what == 1){
                roleList = (List<RoleListBean.Data>) msg.obj;

                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<roleList.size();i++){
                    dataset.add(roleList.get(i).getDep_name());
                }

                spinner_dep.attachDataSource(dataset);

            }else if (msg.what == 2){
                cateList = (List<CateListBean.Data>) msg.obj;

                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<cateList.size();i++){
                    dataset.add(cateList.get(i).getCat_name());
                }
                spinner_type.attachDataSource(dataset);

            }else if (msg.what == 3){
                cateInfoList = (List<CateInfoBean.Data>) msg.obj;
                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<cateInfoList.size();i++){
                    dataset.add(cateInfoList.get(i).getCat_name());
                }
                spinner_class.attachDataSource(dataset);

            }else if (msg.what == 5) {
                cateContentList = (List<CateContentBean.Data>) msg.obj;
                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<cateContentList.size();i++){
                    dataset.add(cateContentList.get(i).getCat_name());
                }
                spinner_info.attachDataSource(dataset);

            }else if (msg.what == 6){
                ressonList = (List<TaskReasonBean.Data>) msg.obj;
                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<ressonList.size();i++){
                    dataset.add(ressonList.get(i).getR_name());
                }
                dataset.add("其它");

                spinner_cancle.attachDataSource(dataset);
            } else if (msg.what == 4){

                final List<AreaListBean.Data> datas = (List<AreaListBean.Data>) msg.obj;
                String[] areaItem = new String[datas.size()];
                dialogIndex = 0;
                for (int i = 0;i<datas.size();i++){
                    areaItem[i] = datas.get(i).getA_name();
                }
                AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
                builder.setTitle("区域");
                builder.setSingleChoiceItems(areaItem, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogIndex = which;
//                        maId = datas.get(which).getA_id();
                    }
                });

                builder.setPositiveButton(whereQuyu.equals("queding") ? "确定" : "下一步", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        maId = datas.get(dialogIndex).getA_id();

                        if (whereQuyu.equals("queding")) {
                            httpTaskPost(taskid, "1");
                        }else if (whereQuyu.equals("zhipai")){
                            httpGetPaiid(mroleId);
                        }else if (whereQuyu.equals("zhuanpai")){
                            dialogZhuanpai();
                        }else if (whereQuyu.equals("fenpai")){
                            dialogFenpai();
                        }
                    }
                });
                builder.setNegativeButton("取消", null);

                //设置对话框是可取消的
                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else if (msg.what == 8){

                final List<ZhiPaiBean.Data> datas = (List<ZhiPaiBean.Data>) msg.obj;
                String[] areaItem = new String[datas.size()];
                dialogIndex = 0;

                for (int i = 0;i<datas.size();i++){
                    areaItem[i] = datas.get(i).getNickname();
                }
                AlertDialog.Builder builder=new AlertDialog.Builder(TaskDetailActivity.this);
                builder.setTitle("指派人");
                builder.setSingleChoiceItems(areaItem, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialogIndex = which;
                    }
                });

                builder.setPositiveButton("指派", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        httpZhipai(datas.get(dialogIndex).getUid());
                    }
                });
                builder.setNegativeButton("取消", null);

                //设置对话框是可取消的
                builder.setCancelable(true);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else if (msg.what == 7){

                bossList =  (List<BossListBean.Data>) msg.obj;

                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<bossList.size();i++){
                    dataset.add(bossList.get(i).getNickname());
                }
                spinner_boss.attachDataSource(dataset);
            }else if (msg.what == 9){
                danweiList = (List<PhoneListBean.Data>) msg.obj;
                List<String> dataset = new ArrayList<String>();
                for (int i = 0;i<danweiList.size();i++){
                    dataset.add(danweiList.get(i).getB_name());
                }
                dataset.add("其它");

                spinner_shigong.attachDataSource(dataset);

            }else if (msg.what == 10){
                List<TaskFilterBean.Data> reasonList = (List<TaskFilterBean.Data>) msg.obj;
                for (int i = 0;i < reasonList.size();i++){
                    if (reasonList.get(i).getStatus().equals("已取消")) {
                        if (reasonList.get(i).getReason() == null){
                            tv_qxreason.setText("无");
                        }else {
                            tv_qxreason.setText(reasonList.get(i).getReason());
                        }
                    }
                }
            }else if (msg.what == 21){

                if (progress.isShowing())
                    progress.dismiss();

                StatusBean.Data data = (StatusBean.Data) msg.obj;
                if (mstatus.equals(data.getStatus())){
                    if (clickIt.equals("sure")) {
                        sureClick();
                    }else if (clickIt.equals("needsp")){
                        needspClick();
                    }else if (clickIt.equals("turnsend")){
                        whereQuyu = "zhuanpai";
                        httpGetArea(mroleId);
                    }else if (clickIt.equals("fenpai")){
                        whereQuyu = "fenpai";
                        httpGetArea(mroleId);
                    }else if (clickIt.equals("cancel")){
                        cancleClick();
                    }
                }else {
                    new MaterialDialog.Builder(TaskDetailActivity.this)
                            .content("当前任务状态已改变，请刷新任务")
                            .positiveText("确定")
                            .dismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    setResult(3);
                                    finish();
                                }
                            })
                            .show();
                }
            }

        return false;
        }
    });


    private void httpGetTask(String taskId){
        progress = new MaterialDialog.Builder(TaskDetailActivity.this)
                .title("加载中")
                .content("请等待")
                .progress(true,0)
                .show();

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);

        GsonRequest<TaskDetailBean> gsonRequest = new GsonRequest<TaskDetailBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKDETAIL, TaskDetailBean.class, params, new com.android.volley.Response.Listener<TaskDetailBean>() {
            @Override
            public void onResponse(TaskDetailBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    progress.dismiss();
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetStatus(String taskId){
        progress = new MaterialDialog.Builder(TaskDetailActivity.this)
                .title("加载中")
                .content("请等待")
                .progress(true,0)
                .show();

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);

        GsonRequest<StatusBean> gsonRequest = new GsonRequest<StatusBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKDETAIL, StatusBean.class, params, new com.android.volley.Response.Listener<StatusBean>() {
            @Override
            public void onResponse(StatusBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 21;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    progress.dismiss();
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //任务状态设置
    private void httpTaskPost(String taskId,String status){
        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);
        params.put("status",status);
        params.put("uid", getCache().getAsString(keyUid));

        if (!cancleReason.equals("")){
            params.put("reason",cancleReason);
        }else if (!mXiucontent.equals("")){
            params.put("reason",mXiucontent);
        }

        params.put("b_id",mbId);
        params.put("a_id",maId);
        if (arceArray != null){
            params.put("more",arceArray + "");
        }
        params.put("xiuTime",mXiutime);
        params.put("b_person",mbPerson);
        params.put("b_phone",mbPhone);
        params.put("b_name",mbname);
        if (box_aqyh.isChecked() & box_tfxq.isChecked()){
            params.put("source", "4");
        }else if (box_aqyh.isChecked()) {
            params.put("source", "2");
        }else if (box_tfxq.isChecked()){
            params.put("source","3");
        }

        Map<String, String> file = new HashMap<String, String>();
        for (int i = 1;i <= totalImages.size();i++){
            file.put("myFile" + i,totalImages.get(i-1));
        }

        //构建请求
        final Request request = new Request.Builder()
                .url(HTTP_TASKTURNSEND)//地址
                .post(new OkHttpPicManager().getBody(params, file))//添加请求体
                .build();

        //        发送异步请求，同步会报错，Android4.0以后禁止在主线程中进行耗时操作

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                showToast( "反馈失败");
                Log.d("ji=========",e + "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) {
                try {

                    try {
                        JSONObject object = new JSONObject(response.body().string());

                        if (object.getString("code").equals("0")){
                            setResult(3);
                            finish();
                        }else {
                            Looper.prepare();
                            showToast(object.getString("msg"));
                            Looper.loop();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {

                    e.printStackTrace();

                }
            }
        });


//        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKTURNSEND, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
//            @Override
//            public void onResponse(BaseBean response) {
//
//                if (response.isSuccess()){
//
//
//                }else {
//                    showToast(response.getMsg());
//                }
//            }
//        }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("result=====",error + "");
//            }
//        });
//
//        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //转派
    private void httpTaskTurn(String taskId){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);
        params.put("uid", getCache().getAsString(keyUid));
        params.put("dep_id",turnRoleId);
        params.put("cat_id",mcatId);
        params.put("ci_id",mciId);
        params.put("cis_id",mcisId);
        params.put("reason",et_reason.getText().toString());
        params.put("a_id",maId);


        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKZHUAN, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){
                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //fen派
    private void httpTaskFen(String taskId){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);
        params.put("uid", getCache().getAsString(keyUid));
        params.put("dep_id",turnRoleId);
        params.put("cat_id",mcatId);
        params.put("ci_id",mciId);
        params.put("cis_id",mcisId);
        params.put("reason",et_reason.getText().toString());
        params.put("a_id",maId);


        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKFEN, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){
                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }



    //恢复
    private void httpHuifu(String taskId){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);
        params.put("uid",getCache().getAsString(keyUid));

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKHUIFU, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){
                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //审批
    private void httpTaskShenpi(String taskId,String state,String shenpiReason){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskId);
        params.put("uid", getCache().getAsString(keyUid));
        params.put("reason",shenpiReason);
        //1 通过 2 驳回
        params.put("state",state);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKSHEN, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){

                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }
    //获取取消原因
    public void httpGetRizhi(String taskid){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskid);

        GsonRequest<TaskFilterBean> gsonRequest = new GsonRequest<TaskFilterBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKLOG, TaskFilterBean.class, params, new com.android.volley.Response.Listener<TaskFilterBean>() {
            @Override
            public void onResponse(TaskFilterBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 10;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {


                    AppLogger.d(response.getMsg());
//                    showToast(tv_time,response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    //审批
    private void httpUpTaskShenpi(String piId,String shenpiReason){

        Map<String,String> params = new HashMap<String, String>();
        params.put("uppi",piId);
        params.put("uid", getCache().getAsString(keyUid));
        params.put("task_id",taskid);
        params.put("reason",shenpiReason);


        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_UPPI, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){

                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }



    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (position > 0) {
                if (parent.getId() == R.id.spinner_dep) {
                    turnRoleId = roleList.get(position-1).getDep_id();
                    httpGetCate(roleList.get(position-1).getDep_id());
                } else if (parent.getId() == R.id.spinner_type) {
                    mcatId = cateList.get(position-1).getCat_id();
                    httpGetCateInfo(cateList.get(position-1).getCat_id());
                } else if (parent.getId() == R.id.spinner_class) {
                    mciId = cateInfoList.get(position-1).getCat_id();
                    httpGetCateContent(mciId);
                } else if (parent.getId() == R.id.spinner_info){
                    mcisId = cateContentList.get(position-1).getCat_id();
                } else if (parent.getId() == R.id.spinner_boss){
                   mBossId =  bossList.get(position - 1).getUid();
                } else if (parent.getId() == R.id.spinner_shigong){
                    if (position - 1 == danweiList.size()){
                        layout_dwname.setVisibility(View.VISIBLE);
                        layout_dwpeople.setVisibility(View.VISIBLE);
                        layout_dwphone.setVisibility(View.VISIBLE);

                        et_dwname.setText("");
                        et_dwpeople.setText("");
                        et_dwphone.setText("");

                    }else {
                        layout_dwname.setVisibility(View.GONE);
                        layout_dwpeople.setVisibility(View.VISIBLE);
                        layout_dwphone.setVisibility(View.VISIBLE);

                        et_dwname.setText(danweiList.get(position - 1).getB_name());
                        et_dwpeople.setText(danweiList.get(position - 1).getB_person());
                        et_dwphone.setText(danweiList.get(position - 1).getB_phone());
                    }
                }
                else {

                    et_otherReason.setVisibility(View.VISIBLE);

                    if (position - 1 != ressonList.size()){
                        cancleReason = ressonList.get(position - 1).getR_name();
                        et_otherReason.setText(cancleReason);
                        et_otherReason.requestFocus();
                    }else {
                        et_otherReason.setText("");
                        et_otherReason.requestFocus();
                    }

                }
            }else {

                if (parent.getId() == R.id.spinner_shigong){
                    layout_dwname.setVisibility(View.GONE);
                    layout_dwpeople.setVisibility(View.GONE);
                    layout_dwphone.setVisibility(View.GONE);

                    et_dwname.setText("");
                    et_dwpeople.setText("");
                    et_dwphone.setText("");
                }else if (parent.getId() == R.id.spinner_cancle){
                    et_otherReason.setVisibility(View.GONE);
                    cancleReason = "";
                }

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    //判断是否有指派权限
    private void httpCanPai(String userid) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("uid", userid);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CANPAI, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()) {

                    btn_zhipai.setVisibility(View.VISIBLE);

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    //判断是否有指派权限
    private void httpCanZhuan(String userid) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("uid", userid);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CANZHUAN, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()) {

                    btn_turnsend.setVisibility(View.VISIBLE);

                }else {
                    AppLogger.d(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //判断是否有分派权限
    private void httpCanFen(String userid) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("uid", userid);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CANFEN, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()) {

                    btn_fenpai.setVisibility(View.VISIBLE);

                }else {
                    AppLogger.d(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获得指派人
    private void httpGetPaiid(String depid) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("dep_id", depid);
        GsonRequest<ZhiPaiBean> gsonRequest = new GsonRequest<ZhiPaiBean>(com.android.volley.Request.Method.POST, Constans.HTTP_PAIID, ZhiPaiBean.class, params, new com.android.volley.Response.Listener<ZhiPaiBean>() {
            @Override
            public void onResponse(ZhiPaiBean response) {

                if (response.isSuccess()) {
                    Message message = new Message();
                    message.what = 8;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                } else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpZhipai(String pai_id) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("uid", getCache().getAsString(keyUid));
        params.put("pai_id", pai_id);
        params.put("task_id", taskid);
        params.put("a_id", maId);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_ZHIPAI, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()) {
                    setResult(3);
                    finish();
                } else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }



    //获得部门列表
    private void httpGetRole(){
        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",getCache().getAsString(keyDepid));

        GsonRequest<RoleListBean> gsonRequest = new GsonRequest<RoleListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_ZHUANDEP, RoleListBean.class, params, new com.android.volley.Response.Listener<RoleListBean>() {
            @Override
            public void onResponse(RoleListBean response) {

                if (response.isSuccess()) {
                    Message message = new Message();
                    message.what = 1;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetCate(String roleid){

        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",roleid);

        GsonRequest<CateListBean> gsonRequest = new GsonRequest<CateListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATELIST, CateListBean.class, params, new com.android.volley.Response.Listener<CateListBean>() {
            @Override
            public void onResponse(CateListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 2;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetCateInfo(String catid){
        Map<String,String> params = new HashMap<String, String>();
        params.put("cat_id",catid);

        GsonRequest<CateInfoBean> gsonRequest = new GsonRequest<CateInfoBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATEINFO, CateInfoBean.class, params, new com.android.volley.Response.Listener<CateInfoBean>() {
            @Override
            public void onResponse(CateInfoBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 3;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetCateContent(String ciid){


        Map<String,String> params = new HashMap<String, String>();
        params.put("ci_id",ciid);
        GsonRequest<CateContentBean> gsonRequest = new GsonRequest<CateContentBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATECONTENT, CateContentBean.class, params, new com.android.volley.Response.Listener<CateContentBean>() {
            @Override
            public void onResponse(CateContentBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 5;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetArea(String depid){
        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",depid);
        params.put("uid",getCache().getAsString(keyUid));

        GsonRequest<AreaListBean> gsonRequest = new GsonRequest<AreaListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_AREALIST, AreaListBean.class, params, new com.android.volley.Response.Listener<AreaListBean>() {
            @Override
            public void onResponse(AreaListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 4;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //删除任务
    private void httpDeletTask(){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskid);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKDELET, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){

                    setResult(3);
                    finish();

                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获得取消原因
    private void httpGetReason() {
        GsonRequest<TaskReasonBean> gsonRequest = new GsonRequest<TaskReasonBean>(com.android.volley.Request.Method.GET, Constans.HTTP_TASKCANCLE, TaskReasonBean.class, null, new com.android.volley.Response.Listener<TaskReasonBean>() {
            @Override
            public void onResponse(TaskReasonBean response) {

                if (response.isSuccess()) {
                    Message message = new Message();
                    message.what = 6;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                } else {
                    showToast(response.getMsg());

                    ressonList = new ArrayList<TaskReasonBean.Data>();
                    List<String> dataset = new ArrayList<String>();
                    dataset.add("其它");
                    spinner_cancle.attachDataSource(dataset);
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });
        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //审批人列表
    public void httpGetBoss(){

        Map<String,String> params = new HashMap<String, String>();
        params.put("uid", getCache().getAsString(Constans.keyUid));

        GsonRequest<BossListBean> gsonRequest = new GsonRequest<BossListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_BOSSLIST, BossListBean.class, params, new com.android.volley.Response.Listener<BossListBean>() {
            @Override
            public void onResponse(BossListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 7;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);


                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    //获取施工单位
    private void httpGetBuilder(){

        GsonRequest<PhoneListBean> gsonRequest = new GsonRequest<PhoneListBean>(com.android.volley.Request.Method.GET, Constans.HTTP_BULDERLIST, PhoneListBean.class, null, new com.android.volley.Response.Listener<PhoneListBean>() {
            @Override
            public void onResponse(PhoneListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 9;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
//                    mDialog.dismiss();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }



        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
          if (resultCode == RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
            if (data != null) {

                ArrayList<String> mImages = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                totalImages.addAll(mImages);

                showMedia(totalImages);
            }
        }else if (requestCode == 3 & resultCode == RESULT_OK){
            ArrayList<String>  mImages = data.getStringArrayListExtra("imglist");
            totalImages = mImages;
            showMedia(totalImages);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
