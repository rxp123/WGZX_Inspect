package com.zzhrtech.wgzx_inspect.ui.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.disease.CateContentBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateInfoBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by renxiangpeng on 2017/1/8.
 */

public class TaskAddAreaage {

//    LinearLayout mcontentView;
    String mdepId;
    Context mContext;
    NiceSpinner spinner_type,spinner_class,spinner_content;
    EditText et_arec;

    Button btn_sure,btn_cancle;
    private List<CateListBean.Data> cateList;
    private List<CateInfoBean.Data> cateInfoList;
    private List<CateContentBean.Data> cateContentList;

    private String mcatId = "",mciId = "",mcisId = "";
    public TaskAddAreaage(Context context,String depid){
//        mcontentView = view;
        mdepId = depid;
        mContext = context;
    }

    public View initView(){
        View inflater = LayoutInflater.from(mContext).inflate(R.layout.layout_addarceage,null);
        spinner_type = (NiceSpinner) inflater.findViewById(R.id.spinner_type);
        spinner_class = (NiceSpinner) inflater.findViewById(R.id.spinner_class);
        spinner_content = (NiceSpinner) inflater.findViewById(R.id.spinner_content);
        btn_sure = (Button) inflater.findViewById(R.id.btn_sure);
        btn_cancle = (Button) inflater.findViewById(R.id.btn_cancle);
        et_arec = (EditText) inflater.findViewById(R.id.et_arec);
        spinner_type.setOnItemSelectedListener(spinnerListener);
        spinner_class.setOnItemSelectedListener(spinnerListener);
        spinner_content.setOnItemSelectedListener(spinnerListener);

        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
//        mcontentView.addView(inflater);
        httpGetCate(mdepId);

        return inflater;
    }


    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                if (parent.getId() == R.id.spinner_type) {
                    mcatId = cateList.get(position-1).getCat_id();
                    httpGetCateInfo(cateList.get(position-1).getCat_id());
                } else if (parent.getId() == R.id.spinner_class) {
                    mciId = cateInfoList.get(position-1).getCat_id();
                    httpGetCateContent(mciId);
                } else if (parent.getId() == R.id.spinner_info){
                    mcisId = cateContentList.get(position-1).getCat_id();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };




    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what){
                case 0:
                    cateList = (List<CateListBean.Data>) msg.obj;
                    List<String> dataset = new ArrayList<String>();
                    for (int i = 0;i<cateList.size();i++){
                        dataset.add(cateList.get(i).getCat_name());
                    }
                    spinner_type.attachDataSource(dataset);
                break;
                case 1:
                    cateInfoList = (List<CateInfoBean.Data>) msg.obj;
                    dataset = new ArrayList<String>();
                    for (int i = 0;i<cateInfoList.size();i++){
                        dataset.add(cateInfoList.get(i).getCat_name());
                    }
                    spinner_class.attachDataSource(dataset);
                    break;
                case 2:

                    cateContentList = (List<CateContentBean.Data>) msg.obj;
                    dataset = new ArrayList<String>();
                    for (int i = 0;i<cateContentList.size();i++){
                        dataset.add(cateContentList.get(i).getCat_name());
                    }
                    spinner_content.attachDataSource(dataset);
                    break;
            }

            return false;
        }
    });


    //获取类型
    private void httpGetCate(String roleid){

        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",roleid);

        GsonRequest<CateListBean> gsonRequest = new GsonRequest<CateListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATELIST, CateListBean.class, params, new com.android.volley.Response.Listener<CateListBean>() {
            @Override
            public void onResponse(CateListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获取种类
    private void httpGetCateInfo(String catid){
        Map<String,String> params = new HashMap<String, String>();
        params.put("cat_id",catid);

        GsonRequest<CateInfoBean> gsonRequest = new GsonRequest<CateInfoBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATEINFO, CateInfoBean.class, params, new com.android.volley.Response.Listener<CateInfoBean>() {
            @Override
            public void onResponse(CateInfoBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 1;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg() + "");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获取内容
    private void httpGetCateContent(String ciid){


        Map<String,String> params = new HashMap<String, String>();
        params.put("ci_id",ciid);
        GsonRequest<CateContentBean> gsonRequest = new GsonRequest<CateContentBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATECONTENT, CateContentBean.class, params, new com.android.volley.Response.Listener<CateContentBean>() {
            @Override
            public void onResponse(CateContentBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 2;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg() + "");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }
}
