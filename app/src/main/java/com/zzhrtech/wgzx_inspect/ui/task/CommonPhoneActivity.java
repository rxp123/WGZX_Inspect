package com.zzhrtech.wgzx_inspect.ui.task;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.PhoneListAdapter;
import com.zzhrtech.wgzx_inspect.base.OnCommCliskListener;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.task.PhoneListBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskDetailBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CommonPhoneActivity extends BaseActivity {

    RecyclerView rv_phone;
    PhoneListAdapter listAdapter;
    private List<PhoneListBean.Data> listBeanList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_phone);

        toolbar.setNavigationIcon(R.mipmap.ic_close);
        initView();

        httpGetBuilder();
    }

    private void initView(){
        rv_phone = (RecyclerView) findViewById(R.id.rv_phone);
        rv_phone.setLayoutManager(new LinearLayoutManager(this));
        rv_phone.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new PhoneListAdapter();
        rv_phone.setAdapter(listAdapter);

        listAdapter.setOnItemClickLitener(new OnCommCliskListener() {
            @Override
            public void onItemClick(View view, final int position, final TextView name, final EditText person, final EditText phone) {
                new MaterialDialog.Builder(CommonPhoneActivity.this)
                        .title("确定派修")
                        .content("请确定已经联系施工单位")
                        .negativeText("取消")
                        .positiveText("确定")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent intent = new Intent();
                                intent.putExtra("bid",listBeanList.get(position).getB_id());
                                intent.putExtra("bperson",person.getText().toString());
                                intent.putExtra("bphone",phone.getText().toString());
                                intent.putExtra("bname",name.getText().toString());
                                setResult(RESULT_OK,intent);
                                finish();
                            }
                        })
                        .show();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.upload) {

                    AlertDialog.Builder builder=new AlertDialog.Builder(CommonPhoneActivity.this);
                    View view = getLayoutInflater().inflate(R.layout.dialog_common,null);

                    final EditText et_name = (EditText) view.findViewById(R.id.et_name);
                    final EditText et_person = (EditText) view.findViewById(R.id.et_person);
                    final EditText et_phone = (EditText) view.findViewById(R.id.et_phone);

                    builder.setView(view);

                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.putExtra("bid","-1");
                            intent.putExtra("bname",et_name.getText().toString());
                            intent.putExtra("bperson",et_person.getText().toString());
                            intent.putExtra("bphone",et_phone.getText().toString());
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    });
                    builder.setNegativeButton("取消", null);

                    //设置对话框是可取消的
                    builder.setCancelable(true);

                    AlertDialog dialog = builder.create();

                    dialog.show();
                }
            return true;
        }
    });


    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            listBeanList = (List<PhoneListBean.Data>) msg.obj;
            for (PhoneListBean.Data bean : listBeanList){
                listAdapter.addItem(bean);
            }

            return false;
        }
    });


    private void httpGetBuilder(){

        GsonRequest<PhoneListBean> gsonRequest = new GsonRequest<PhoneListBean>(Request.Method.GET, Constans.HTTP_BULDERLIST, PhoneListBean.class, null, new com.android.volley.Response.Listener<PhoneListBean>() {
            @Override
            public void onResponse(PhoneListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
//                    mDialog.dismiss();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_common, menu);
        return true;
    }
}
