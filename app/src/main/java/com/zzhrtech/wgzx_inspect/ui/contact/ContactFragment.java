package com.zzhrtech.wgzx_inspect.ui.contact;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskPagerAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.beans.disease.CateAllBean;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.ui.me.MeFragment;
import com.zzhrtech.wgzx_inspect.utils.HuanxinHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends BaseFragment {

    private SegmentTabLayout tabLayout;
    private ViewPager vp_contact;
    private Toolbar mtoolBar;
    private String[] mTitles;
    private ArrayList<Fragment> mFragments = new ArrayList<>();


    ConversationFragment conversationFragment;
    public ContactFragment() {
        // Required empty public constructor
    }

    public static ContactFragment newInstance() {

        Bundle args = new Bundle();

        ContactFragment fragment = new ContactFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_contact, container, false);

        tabLayout = (SegmentTabLayout) view.findViewById(R.id.tl_2);
        vp_contact = (ViewPager) view.findViewById(R.id.vp_contact);
        mtoolBar = (Toolbar)view.findViewById(R.id.toolbar);

        return view;
    }

    @Override
    protected void initData() {
        super.initData();
        mTitles = new String[]{getResources().getString(R.string.xiaoxi), "联系人"};

        conversationFragment = ConversationFragment.newInstance("","");
        mFragments.add(conversationFragment);
        mFragments.add(ContactallFragment.newInstance());


        TaskPagerAdapter pagerAdapter = new TaskPagerAdapter(getChildFragmentManager(),mFragments,mTitles);
        vp_contact.setAdapter(pagerAdapter);
        tabLayout.setTabData(mTitles);
        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vp_contact.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        vp_contact.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }







}
