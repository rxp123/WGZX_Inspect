package com.zzhrtech.wgzx_inspect.ui.task;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupWindow;

import com.flyco.tablayout.SlidingTabLayout;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskPagerAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.ui.contact.ContactFragment;
import com.zzhrtech.wgzx_inspect.ui.me.LoginActivity;

import java.lang.reflect.Field;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragment extends BaseFragment {

    //巡查员
    private final String[] mTitles = {
            "待确认", "已确认", "已取消"
            , "已派修", "已反馈", "已发布"
    };
    private final int[] mStatus = {
            0,1,2
            ,4,5,0
    };

    //管理员
    private final String[] mTitlesmg = {
            "待确认", "已确认", "已取消"
            , "已派修", "已反馈","待审批","已审批", "已发布"
    };
    private final int[] mStatusmg = {
            0,1,2
            ,4,5,6,7,0
    };

    private String areaId = "",qudaoId = "";
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private TaskPagerAdapter pagerAdapter;
    private ViewPager vp_task;
    private SlidingTabLayout tabLayout;
    public TaskFragment() {
        // Required empty public constructor
    }

    public static TaskFragment newInstance() {

        Bundle args = new Bundle();

        TaskFragment fragment = new TaskFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View view =  inflater.inflate(R.layout.fragment_task, container, false);

        vp_task = (ViewPager) view.findViewById(R.id.vp_task);
        tabLayout = (SlidingTabLayout) view.findViewById(R.id.tl_1);

        return view;
    }


    @Override
    protected void initData() {
        super.initData();

        if (((MainActivity) mActivity).getCache().getAsString(Constans.keyIsUserboss).equals("1")) {
            for (int i = 0; i < mStatusmg.length; i++) {
                mFragments.add(TaskChildFragment.newInstance(mStatusmg[i], mTitlesmg[i],this));
            }
            pagerAdapter = new TaskPagerAdapter(getChildFragmentManager(),mFragments,mTitlesmg);
        }else {
            for (int i = 0; i < mStatus.length; i++) {
                mFragments.add(TaskChildFragment.newInstance(mStatus[i], mTitles[i],this));
            }
            pagerAdapter = new TaskPagerAdapter(getChildFragmentManager(),mFragments,mTitles);
        }


        vp_task.setAdapter(pagerAdapter);

        tabLayout.setViewPager(vp_task);

    }

    public void setMsg(String title,int num){

        for (int i = 0;i < mTitles.length;i++){
            if (mTitles[i].equals(title)){
                if (num <= 0){
                    tabLayout.hideMsg(i);
                }else {
                    tabLayout.showMsg(i, num);
                }
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1 & resultCode == 3){

            pagerAdapter.notifyDataSetChanged();

        }else if (requestCode == 4 & resultCode == Activity.RESULT_OK){

            setAreaId(data.getStringExtra("areaid"));
            setQudaoId(data.getStringExtra("qudaoid"));
            pagerAdapter.notifyDataSetChanged();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getQudaoId() {
        return qudaoId;
    }

    public void setQudaoId(String qudaoId) {
        this.qudaoId = qudaoId;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.menu_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter){

            Intent intent = new Intent(mActivity,TaskFilterActivity.class);
            intent.putExtra("areaId",areaId);
            intent.putExtra("qudaoId",qudaoId);
            startActivityForResult(intent,4);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
