package com.zzhrtech.wgzx_inspect.ui.me;

import android.app.ProgressDialog;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;

public class MonitorActivity extends BaseActivity {

    private VideoView v;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);
        toolbar.setNavigationIcon(R.mipmap.ic_close);

        v = (VideoView) findViewById(R.id.video_view);

        playvideo(getIntent().getStringExtra("path"));
    }

//    MediaController mediaController;
    MaterialDialog progress;

    //播放视频
    public void playvideo(String videopath) {
        try {

            progress = new MaterialDialog.Builder(MonitorActivity.this)
                    .title("")
                    .content("加载视频")
                    .progress(true,0)
                    .show();
            getWindow().setFormat(PixelFormat.TRANSLUCENT);

//            mediaController = new MediaController(MonitorActivity.this);

            Uri video = Uri.parse(videopath);
//            v.setMediaController(mediaController);
            v.setVideoURI(video);


            v.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer player) {

                    try {
                        player.start();
                    } catch (Exception e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                    progress.dismiss();
                    v.start();
                }
            });

        } catch (Exception e) {
            progress.dismiss();
            System.out.println("Video Play Error :" + e.getMessage());
        }

    }
}
