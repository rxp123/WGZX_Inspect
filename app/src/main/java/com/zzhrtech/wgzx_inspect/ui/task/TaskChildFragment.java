package com.zzhrtech.wgzx_inspect.ui.task;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.aspsine.swipetoloadlayout.OnLoadMoreListener;
import com.aspsine.swipetoloadlayout.OnRefreshListener;
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout;
import com.dinuscxj.refresh.RecyclerRefreshLayout;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskListAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.disease.CateListBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskChildFragment extends BaseFragment implements OnRefreshListener, OnLoadMoreListener {

    private List<TaskListBean.Data> dataList;

    private TaskListAdapter listAdapter;
    private RecyclerView rc_task;
    private int mstatus;
    private String mtitle;
    private String mqudaoId,mareaId;
    private SwipeToLoadLayout swipeToLoadLayout;

    private TextView emptyText;
    private boolean viewReady = false;

    public TaskChildFragment() {
        // Required empty public constructor
    }


    public static TaskChildFragment newInstance(int status,String title,Fragment mfragment) {

        Bundle args = new Bundle();
        args.putInt("status", status);
        args.putString("title", title);
        TaskChildFragment fragment = new TaskChildFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_task_child, container, false);

        viewReady = true;
        rc_task = (RecyclerView) view.findViewById(R.id.swipe_target);
        emptyText = (TextView) view.findViewById(R.id.emptyText);
        swipeToLoadLayout = (SwipeToLoadLayout) view.findViewById(R.id.swipeToLoadLayout);

        rc_task.setLayoutManager(new LinearLayoutManager(mActivity));
        rc_task.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new TaskListAdapter();
        rc_task.setAdapter(listAdapter);


        swipeToLoadLayout.setOnRefreshListener(this);
        swipeToLoadLayout.setOnLoadMoreListener(this);

        listAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(mActivity,TaskDetailActivity.class);
                intent.putExtra("taskid",dataList.get(position).getTask_id());
                intent.putExtra("status",mstatus + "");
                intent.putExtra("tabtitle",mtitle);
                intent.putExtra("minSource",dataList.get(position).getSource());
                getParentFragment().startActivityForResult(intent,1);
            }
        });
        return view;
    }

    @Override
    protected void initData() {
        super.initData();
        if (getArguments() != null){
            mstatus = getArguments().getInt("status");
            mtitle = getArguments().getString("title");
        }

        mareaId = ((TaskFragment) getParentFragment()).getAreaId();
        mqudaoId = ((TaskFragment) getParentFragment()).getQudaoId();

        swipeToLoadLayout.setRefreshing(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if (!viewReady)
            return;

        if (isVisibleToUser){
            swipeToLoadLayout.setRefreshing(true);
        }

        super.setUserVisibleHint(isVisibleToUser);
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == 0) {
                emptyText.setVisibility(View.GONE);
                swipeToLoadLayout.setRefreshing(false);
                dataList = (List<TaskListBean.Data>) msg.obj;
                for (TaskListBean.Data data : dataList) {
                    listAdapter.addItem(data);
                }

                ((TaskFragment) getParentFragment()).setMsg(mtitle,dataList.size());
            }
            return false;
        }
    });

    public void httpGetTasks(String status){
        Map<String,String> params = new HashMap<String, String>();
        if (mtitle.equals("已发布")) {
            params.put("fa","1");
        }else {
            params.put("status",status);
        }
        params.put("uid", ((MainActivity) mActivity).getCache().getAsString(Constans.keyUid));

        params.put("dep_id",((MainActivity) mActivity).getCache().getAsString(Constans.keyDepid));
        params.put("a_id",mareaId);
        params.put("source",mqudaoId);
        //判断是不是领导账号
        if (((MainActivity) mActivity).getCache().getAsString(Constans.keyIsboss).equals("1")){
            params.put("isboss","1");
        }


        AppLogger.d(params + "");

        GsonRequest<TaskListBean> gsonRequest = new GsonRequest<TaskListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKLIST, TaskListBean.class, params, new com.android.volley.Response.Listener<TaskListBean>() {
            @Override
            public void onResponse(TaskListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);

                }else {
                    swipeToLoadLayout.setRefreshing(false);
                    listAdapter.notifyDataSetChanged();
                    emptyText.setVisibility(View.VISIBLE);
                    ((TaskFragment) getParentFragment()).setMsg(mtitle,0);
//                    showToast(tv_time,response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeToLoadLayout.setRefreshing(false);
                emptyText.setVisibility(View.VISIBLE);
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (swipeToLoadLayout.isRefreshing()) {
            swipeToLoadLayout.setRefreshing(false);
        }
        if (swipeToLoadLayout.isLoadingMore()) {
            swipeToLoadLayout.setLoadingMore(false);
        }
    }

    @Override
    public void onRefresh() {

        listAdapter.cleanList();

        httpGetTasks(mstatus + "");

    }

    @Override
    public void onLoadMore() {

        swipeToLoadLayout.setLoadingMore(false);
    }
}
