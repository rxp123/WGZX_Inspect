package com.zzhrtech.wgzx_inspect.ui.me;

import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.beans.task.BaseBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.Map;

public class ModifyPwdActivity extends BaseActivity {

    EditText et_oldpwd,et_newpwd,et_newpwd2;
    Button btn_modify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_pwd);
        toolbar.setNavigationIcon(R.mipmap.ic_close);

        initView();
    }
    private void initView(){
        et_oldpwd = (EditText) findViewById(R.id.oldpwd);
        et_newpwd = (EditText) findViewById(R.id.newpwd);
        et_newpwd2 = (EditText) findViewById(R.id.newpwd2);
        btn_modify = (Button) findViewById(R.id.btn_modify);

        btn_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    private void httpModify(String oldpass,String newpass){
        Map<String,String> params = new HashMap<String, String>();
        params.put("uid",getCache().getAsString(keyUid));
        params.put("oldPass",oldpass);
        params.put("newPass",newpass);

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CHPASS, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){
                    finish();
                    showToast("已修改");
                }else {
                    showToast(response.getMsg());
//                    mDialog.dismiss();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    private void attemptLogin() {

        // Reset errors.
        et_oldpwd.setError(null);
        et_newpwd.setError(null);
        et_newpwd2.setError(null);

        // Store values at the time of the login attempt.
        String oldpwd = et_oldpwd.getText().toString();
        String newpwd = et_newpwd.getText().toString();
        String newpwd2 = et_newpwd2.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(oldpwd) && !isPasswordValid(oldpwd)) {
            et_oldpwd.setError(getString(R.string.error_invalid_password));
            focusView = et_oldpwd;
            cancel = true;
        }

        // Check for a valid email address.
        if (!TextUtils.isEmpty(newpwd) && !isPasswordValid(newpwd)) {
            et_newpwd.setError(getString(R.string.error_invalid_password));
            focusView = et_newpwd;
            cancel = true;
        }

        // Check for a valid email address.
        if (!TextUtils.isEmpty(newpwd2) && !isPasswordValid(newpwd2)) {
            et_newpwd2.setError(getString(R.string.error_invalid_password));
            focusView = et_newpwd2;
            cancel = true;
        }

        if (!newpwd.equals(newpwd2)) {
            et_newpwd2.setError("两次密码不一致");
            focusView = et_newpwd2;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            httpModify(oldpwd,newpwd);
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}
