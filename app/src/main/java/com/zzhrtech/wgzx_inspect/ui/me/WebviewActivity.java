package com.zzhrtech.wgzx_inspect.ui.me;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.tencent.TIMConversation;
import com.tencent.TIMGroupCacheInfo;
import com.tencent.TIMMessage;
import com.tencent.qcloud.presentation.presenter.ConversationPresenter;
import com.tencent.qcloud.presentation.viewfeatures.ConversationView;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.ui.contact.ChatActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class WebviewActivity extends BaseActivity {

    private String url;
    private WebView webView;
    private MaterialDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        toolbar.setNavigationIcon(R.mipmap.ic_close);
        url = getIntent().getStringExtra("url");
        setTitle(getIntent().getStringExtra("title"));
        initView();
        webView.loadUrl(url);
    }

    private void initView(){
        webView = (WebView) findViewById(R.id.webView);

        progress = new MaterialDialog.Builder(WebviewActivity.this)
                .content("加载中...")
                .progress(true,0)
                .show();

        webView.freeMemory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);// 让浏览器来存储页面元素的DOM模型，从而使JavaScript可以执行操作了。
        webView.getSettings().setLoadsImagesAutomatically(true);//自动加载图片
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);//自动适应屏幕
        // 设置可以支持缩放
        webView.getSettings().setSupportZoom(true);
// 设置出现缩放工具
        webView.getSettings().setBuiltInZoomControls(true);

        webView.requestFocus();

        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                if (newProgress == 100){
                    progress.dismiss();
                }else {
                    if (!progress.isShowing()){
                        progress.show();
                    }
                }

                super.onProgressChanged(view, newProgress);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.startsWith("http:") || url.startsWith("https:")) {
                    view.loadUrl(url);
                    return false;
                }
                return true;
            }
        });

//        Log.d("fs========",Text() + "");


    }


}
