package com.zzhrtech.wgzx_inspect.ui.task;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskFilterAdapter;
import com.zzhrtech.wgzx_inspect.beans.disease.AreaAllListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class
TaskFilterActivity extends BaseActivity {

//    RecyclerView recy_qudao,recy_quyu;
    String[] qudaos = new String[]{"内部员工","电话和微博","应急突发险情","重大安全隐患","应急和重大安全隐患","市民"};
    String[] qudaoIds = new String[]{"0","1","2","3","4","5"};
    RadioGroup group_quyu,group_qudao;

    Button btn_cancle,btn_sure;
    private String areaId = "",qudaoId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_filter);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        areaId = getIntent().getStringExtra("areaId");
        qudaoId = getIntent().getStringExtra("qudaoId");

        group_quyu = (RadioGroup) findViewById(R.id.group_quyu);
        group_qudao = (RadioGroup) findViewById(R.id.group_qudao);

        btn_cancle = (Button) findViewById(R.id.btn_cancle);

        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_sure = (Button) findViewById(R.id.btn_sure);

        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("areaid", areaId);
                    intent.putExtra("qudaoid", qudaoId);
                    setResult(RESULT_OK, intent);
                    finish();
            }
        });

        httpGetArea();
    }


    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == 0){

                final List<AreaAllListBean.Data> areaData = (List<AreaAllListBean.Data>) msg.obj;

                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(16,8,16,8);

                RadioButton button = new RadioButton(TaskFilterActivity.this);
                button.setPadding(16,8,16,8);
                button.setLayoutParams(params);
                button.setText("全部区域");
                button.setTag("");
//                button.setChecked(true);
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            areaId = "";
                        }
                    }
                });
                group_quyu.addView(button);

                for (int i = 0;i< areaData.size();i++) {

                    TextView textView = new TextView(TaskFilterActivity.this);
                    textView.setPadding(8,8,8,8);
                    textView.setLayoutParams(params);
                    textView.setTag("title");
                    textView.setText(areaData.get(i).getDep_name());

                    group_quyu.addView(textView);

                    if (areaData.get(i).getArea().size() != 0){


                        for (int a = 0; a< areaData.get(i).getArea().size();a++) {
                            button = new RadioButton(TaskFilterActivity.this);
                            button.setPadding(16, 8, 16, 8);
                            button.setLayoutParams(params);
                            button.setText(areaData.get(i).getArea().get(a).getA_name());
                            button.setTag(areaData.get(i).getArea().get(a).getA_id());
                            final int finalI = i;
                            final int finalA = a;
                            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        areaId = areaData.get(finalI).getArea().get(finalA).getA_id();
                                    }

                                }
                            });

                            group_quyu.addView(button);
                        }


                    }
                }

                for (int i = 0; i < group_quyu.getChildCount(); i++){

                    if (group_quyu.getChildAt(i).getTag().equals(areaId)){
                        ((RadioButton) group_quyu.getChildAt(i)).setChecked(true);
                    }

                }


                //--------------------------
                button = new RadioButton(TaskFilterActivity.this);
                button.setPadding(16,8,16,8);
                button.setLayoutParams(params);
                button.setText("全部渠道");
                button.setTag("");
//                button.setChecked(true);
                button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            qudaoId = "";
                        }
                    }
                });

                group_qudao.addView(button);
                for (int i = 0;i< qudaos.length;i++){
                    button = new RadioButton(TaskFilterActivity.this);
                    button.setPadding(16,8,16,8);
                    button.setLayoutParams(params);
                    button.setText(qudaos[i]);
                    button.setTag(qudaoIds[i]);

                    final int finalI = i;
                    button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked){
                                qudaoId = qudaoIds[finalI];
                            }
                        }
                    });

                    group_qudao.addView(button);
                }


                for (int i = 0; i < group_qudao.getChildCount(); i++){

                    if (group_qudao.getChildAt(i).getTag().equals(qudaoId)){
                        ((RadioButton) group_qudao.getChildAt(i)).setChecked(true);
                    }

                }

            }

            return false;
        }
    });

    private void httpGetArea(){

        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",getCache().getAsString(Constans.keyDepid));
        params.put("uid",getCache().getAsString(Constans.keyUid));

        GsonRequest<AreaAllListBean> gsonRequest = new GsonRequest<AreaAllListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_AREALISTX, AreaAllListBean.class, params, new com.android.volley.Response.Listener<AreaAllListBean>() {
            @Override
            public void onResponse(AreaAllListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
//                    mDialog.dismiss();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }
}
