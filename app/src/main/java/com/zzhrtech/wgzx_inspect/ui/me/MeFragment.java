package com.zzhrtech.wgzx_inspect.ui.me;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.amap.api.maps.AMapException;
import com.amap.api.maps.offlinemap.OfflineMapManager;
import com.amap.api.maps.offlinemap.OfflineMapStatus;
import com.loveplusplus.update.UpdateChecker;
import com.squareup.picasso.Picasso;
import com.tencent.TIMCallBack;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.qcloud.presentation.business.LoginBusiness;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.GuideActivity;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.ui.disease.DiseaseUploadActivity;


import java.util.ArrayList;
/**
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends BaseFragment implements OfflineMapManager.OfflineMapDownloadListener {

    private ImageView civ_head;
    private TextView tv_info,tv_feedback,tv_modify,tv_about,tv_nickname,tv_phone,tv_count,tv_map,tv_mapMark,tv_update,tv_shuoming,tv_voice;

    private OfflineMapManager amapManager;
//    private AutoCompleteTextView tv_progress;
    public MeFragment() {
        // Required empty public constructor
    }

    public static MeFragment newInstance() {

        Bundle args = new Bundle();

        MeFragment fragment = new MeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_me, container, false);

        setHasOptionsMenu(true);
        tv_nickname = (TextView) view.findViewById(R.id.tv_nickname);
        tv_phone = (TextView) view.findViewById(R.id.tv_phone);
        tv_mapMark = (TextView) view.findViewById(R.id.tv_mapMark);
        tv_mapMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,MapPointActivity.class);
                startActivity(intent);
            }
        });
        tv_update = (TextView) view.findViewById(R.id.tv_update);
        tv_count = (TextView) view.findViewById(R.id.tv_count);
        tv_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,ChartActivity.class);
                startActivity(intent);
            }
        });
        civ_head = (ImageView) view.findViewById(R.id.civ_head);
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UpdateChecker.checkForDialog(mActivity,true);
//                Intent intent = new Intent(mActivity,LoginActivity.class);
//                startActivity(intent);
            }
        });
        tv_shuoming = (TextView) view.findViewById(R.id.tv_shuoming);
        tv_shuoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,ShuomingActivity.class);
                startActivity(intent);
            }
        });
        tv_info = (TextView) view.findViewById(R.id.tv_info);
        tv_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,MyInfoActivity.class);
                startActivity(intent);
            }
        });
        tv_voice = (TextView) view.findViewById(R.id.tv_voice);
        tv_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,VoiceActivity.class);
                startActivity(intent);
            }
        });
        tv_feedback = (TextView) view.findViewById(R.id.tv_feedback);
        tv_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,FeedbackActivity.class);
                startActivity(intent);
            }
        });
        tv_modify = (TextView) view.findViewById(R.id.tv_modify);
        tv_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,ModifyPwdActivity.class);
                startActivity(intent);
            }
        });
        tv_about = (TextView) view.findViewById(R.id.tv_about);
        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,WebviewActivity.class);
                intent.putExtra("url",Constans.HTTP_ABOUT);
                intent.putExtra("title","关于我们");
                startActivity(intent);
            }
        });
        tv_map = (TextView) view.findViewById(R.id.tv_map);
        tv_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    amapManager.updateOfflineCityByName("长春市");
                } catch (AMapException e) {
                    e.printStackTrace();
                }

            }
        });

        amapManager = new OfflineMapManager(mActivity, this);

        return view;
    }

    @Override
    protected void initData() {
        super.initData();

        if (((MainActivity) mActivity).getCache().getAsString(Constans.keyHead) != null) {
            if (!((MainActivity) mActivity).getCache().getAsString(Constans.keyHead).isEmpty())
            Picasso.with(mActivity)
                    .load(((MainActivity) mActivity).getCache().getAsString(Constans.keyHead))
                    .placeholder(R.mipmap.ic_placeholde)
                    .into(civ_head);
        }

        tv_nickname.setText(((MainActivity) mActivity).getCache().getAsString(Constans.keyNickname));
        tv_phone.setText(((MainActivity) mActivity).getCache().getAsString(Constans.keyUsername));
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_info, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.quit){

            new MaterialDialog.Builder(mActivity)
                    .content("是否确定退出？")
                    .negativeText("取消")
                    .positiveText("确定")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

//                            LoginBusiness.logout(new TIMCallBack() {
//                                @Override
//                                public void onError(int i, String s) {
//                                    showToast("退出失败");
//                                }
//
//                                @Override
//                                public void onSuccess() {
                            ((MainActivity) mActivity).getCache().clear();
                            LoginBusiness.logout(null);
                            XGPushManager.unregisterPush(getActivity().getApplicationContext());
                            Intent intent = new Intent(mActivity,GuideActivity.class);
                            startActivity(intent);
                            mActivity.finish();
//                                }
//                            }
//                        );
                        }
                    })
                    .show();





        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDownload(int state, int completeCode, String downName) {

        switch (state) {
            case OfflineMapStatus.SUCCESS:

                showToast("完成");
                // changeOfflineMapTitle(OfflineMapStatus.SUCCESS, downName);
                break;
            case OfflineMapStatus.LOADING:
                Log.d("amap-download", "download: " + completeCode + "%" + ","
                        + downName);

//                tv_progress.setText("下载 " + completeCode + "%");

                // changeOfflineMapTitle(OfflineMapStatus.LOADING, downName);
                break;
            case OfflineMapStatus.UNZIP:
//                tv_progress.setText("解压 " + completeCode + "%");
                Log.d("amap-unzip", "unzip: " + completeCode + "%" + "," + downName);
                // changeOfflineMapTitle(OfflineMapStatus.UNZIP);
                // changeOfflineMapTitle(OfflineMapStatus.UNZIP, downName);
                break;
            case OfflineMapStatus.WAITING:
                Log.d("amap-waiting", "WAITING: " + completeCode + "%" + ","
                        + downName);
                break;
            case OfflineMapStatus.PAUSE:
                Log.d("amap-pause", "pause: " + completeCode + "%" + "," + downName);
                break;
            case OfflineMapStatus.STOP:
                break;
            case OfflineMapStatus.ERROR:
                Log.e("amap-download", "download: " + " ERROR " + downName);
                break;
            case OfflineMapStatus.EXCEPTION_AMAP:
                Log.e("amap-download", "download: " + " EXCEPTION_AMAP " + downName);
                break;
            case OfflineMapStatus.EXCEPTION_NETWORK_LOADING:
                Log.e("amap-download", "download: " + " EXCEPTION_NETWORK_LOADING "
                        + downName);
                Toast.makeText(mActivity, "网络异常", Toast.LENGTH_SHORT)
                        .show();
                amapManager.pause();
                break;
            case OfflineMapStatus.EXCEPTION_SDCARD:
                Log.e("amap-download", "download: " + " EXCEPTION_SDCARD "
                        + downName);
                break;
            default:
                break;
        }

    }

    @Override
    public void onCheckUpdate(boolean hasNew, String s) {

        if (hasNew){
            new MaterialDialog.Builder(mActivity)
                    .title("有新的地图")
                    .content("是否更新?")
                    .negativeText("取消")
                    .positiveText("确定")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            try {
                                amapManager.downloadByCityName("长春市");
                            } catch (AMapException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    })
                    .show();

        }else {
            showToast("暂无更新");
        }
    }

    @Override
    public void onRemove(boolean success, String name, String describe) {
        Log.i("amap-demo", "onRemove " + name + " : " + success + " , "
                + describe);
    }
}
