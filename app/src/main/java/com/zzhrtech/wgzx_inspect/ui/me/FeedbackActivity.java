package com.zzhrtech.wgzx_inspect.ui.me;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.beans.task.BaseBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends BaseActivity {

    private Button btn_submit;
    private RadioButton btn_cpjy,btn_cxcw;
    private EditText et_content,et_phone;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_cpjy = (RadioButton) findViewById(R.id.btn_cpjy);
        btn_cxcw = (RadioButton) findViewById(R.id.btn_cxcw);
        et_content = (EditText) findViewById(R.id.et_content);
        et_phone = (EditText) findViewById(R.id.et_phone);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_content.getText().toString().isEmpty()){
                    showToast("请输入内容");
                }else if (et_phone.getText().toString().isEmpty()){
                    showToast("请输入电话");
                }else {
                    if (btn_cpjy.isChecked()) {
                        type = "1";
                    } else {
                        type = "2";
                    }
                    httpFeedBack(type);
                }

            }
        });
    }


    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {


            if (msg.what == 0){

                finish();
            }

            return false;
        }
    });



    private void httpFeedBack(String type){
        Map<String,String> params = new HashMap<String, String>();
        params.put("su_type","1");
        params.put("su_content",et_content.getText().toString());
        params.put("su_phone",et_phone.getText().toString());

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_SUGGEST, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){

                    mHandler.sendEmptyMessage(0);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });
        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_feedback, menu);
        return true;
    }
}
