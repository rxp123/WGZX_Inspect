package com.zzhrtech.wgzx_inspect.ui.disease;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.disease.DiseaseTypeAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.disease.CateAllBean;
import com.zzhrtech.wgzx_inspect.beans.disease.DiseaseTypeBean;
import com.zzhrtech.wgzx_inspect.beans.disease.RoleListBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.ArrayList;
import java.util.List;

import me.iwf.photopicker.PhotoPicker;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DiseaseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiseaseFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView rv_disease;
//    private FloatingActionButton btn_weibo;
    private DiseaseTypeAdapter typeAdapter;
//    private List<DiseaseTypeBean> typeBeanList;
//    private String[] names = {"道路","桥梁","排水","停车","防汛","电话与微博"};

    private List<CateAllBean.Data> cateBean;

    private FloatingActionMenu menu_weibo;
    public DiseaseFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DiseaseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DiseaseFragment newInstance(String param1, String param2) {
        DiseaseFragment fragment = new DiseaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_disease, container, false);

//        btn_weibo = (FloatingActionButton) view.findViewById(R.id.btn_weibo);
        rv_disease = (RecyclerView) view.findViewById(R.id.rv_disease);
        menu_weibo = (FloatingActionMenu) view.findViewById(R.id.menu_weibo);

        rv_disease.setLayoutManager(new GridLayoutManager(mActivity,2));
        rv_disease.setItemAnimator(new DefaultItemAnimator());
        typeAdapter = new DiseaseTypeAdapter();
        rv_disease.setAdapter(typeAdapter);

        typeAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(getActivity(),DiseaseUploadActivity.class);
                intent.putExtra("depid",cateBean.get(position).getDep_id());
                intent.putExtra("catid",cateBean.get(position).getCat_id());
                intent.putExtra("catname",cateBean.get(position).getCat_name());
                intent.putExtra("source","0");
                mActivity.startActivityForResult(intent, 2);

            }
        });

        FloatingActionButton programFab1 = new FloatingActionButton(getActivity());
        programFab1.setButtonSize(FloatingActionButton.SIZE_MINI);
        programFab1.setColorNormalResId(R.color.color1);
        programFab1.setColorPressedResId(R.color.color11);
        programFab1.setColorRippleResId(R.color.color111);
        programFab1.setLabelText("电话");
        programFab1.setImageResource(R.mipmap.ic_dis_phone);
        menu_weibo.addMenuButton(programFab1);

        FloatingActionButton programFab2 = new FloatingActionButton(getActivity());
        programFab2.setButtonSize(FloatingActionButton.SIZE_MINI);
        programFab2.setColorNormalResId(R.color.color1);
        programFab2.setColorPressedResId(R.color.color11);
        programFab2.setColorRippleResId(R.color.color111);
        programFab2.setLabelText("微博");
        programFab2.setImageResource(R.mipmap.ic_dis_sina);

        programFab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),DiseaseUploadActivity.class);
                intent.putExtra("source","1");
                startActivityForResult(intent, 2);
            }
        });


        programFab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),DiseaseUploadActivity.class);
                intent.putExtra("source","1");
                startActivityForResult(intent, 2);
            }
        });

        menu_weibo.addMenuButton(programFab2);
        menu_weibo.setClosedOnTouchOutside(true);


        return view;
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            cateBean = (List<CateAllBean.Data>) msg.obj;

            for (CateAllBean.Data data : cateBean){
                typeAdapter.addItem(data);
            }

            return false;
        }
    });

    @Override
    protected void initData() {
        super.initData();

        GsonRequest<CateAllBean> gsonRequest = new GsonRequest<CateAllBean>(com.android.volley.Request.Method.GET, Constans.HTTP_CATELISTALL, CateAllBean.class, null, new com.android.volley.Response.Listener<CateAllBean>() {
            @Override
            public void onResponse(CateAllBean response) {

                if (response.isSuccess()) {
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
//                    showToast(tv_time,response.getMsg());
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
//                dialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);

    }



}
