package com.zzhrtech.wgzx_inspect.ui.disease;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;

import java.util.ArrayList;
import java.util.List;


public class ImagePickActivity extends BaseActivity {

    private ViewPager viewPager;
    private ArrayList<String> images;
    private List<View> viewList;
    private ImagePagerAdapter pagerAdapter;
    private int mCurrent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pick);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putStringArrayListExtra("imglist",images);
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        mCurrent = getIntent().getIntExtra("current",0);

        images = getIntent().getStringArrayListExtra("images");

        getViewList();

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                getSupportActionBar().setTitle(position + "/" + viewList.size());
                setPage(position+1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setPage(1);

        pagerAdapter = new ImagePagerAdapter(viewList);
        viewPager.setAdapter(pagerAdapter);

        viewPager.setCurrentItem(mCurrent);

    }

    private void getViewList(){
        viewList = new ArrayList<View>();
        for (int i = 0;i < images.size();i++){
            ImageView imageView = new ImageView(this);
            Glide.with(this)
                    .load(images.get(i))
                    .fitCenter()
                    .into(imageView);
            viewList.add(imageView);
        }
    }

    //设置页码
    private void setPage(int current){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        actionBar.setTitle(current + "/" + images.size());

    }

    private class ImagePagerAdapter extends PagerAdapter{

        private List<View> mListViews;
        public ImagePagerAdapter(List<View> viewList){
            mListViews = viewList;
        }
        @Override
        public int getCount() {
            return mListViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mListViews.get(position));
            return mListViews.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
//            super.destroyItem(container, position, object);
            container.removeView(mListViews.get(position));
        }

//        @Override
//        public int getItemPosition(Object object) {
//            return PagerAdapter.POSITION_NONE;
//
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (getIntent().getStringExtra("channel").equals("upload")) {
            getMenuInflater().inflate(R.menu.menu_imagepicker, menu);
        }
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.trash){

            new MaterialDialog.Builder(ImagePickActivity.this)
                    .title("提示")
                    .content("确定删除该照片?")
                    .negativeText("取消")
                    .positiveText("确定")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            int current = viewPager.getCurrentItem();
                            images.remove(current);

                            if (images.size() <= 0){
                                Intent intent = new Intent();
                                intent.putStringArrayListExtra("imglist",images);
                                setResult(RESULT_OK,intent);
                                finish();
                            }else {
                                getViewList();
                                pagerAdapter = new ImagePagerAdapter(viewList);
                                viewPager.setAdapter(pagerAdapter);
                                viewPager.setCurrentItem(current, false);
                                setPage(current + 1);
                            }
                            dialog.dismiss();
                        }
                    })
                    .show();


        }
        return super.onOptionsItemSelected(item);
    }
}
