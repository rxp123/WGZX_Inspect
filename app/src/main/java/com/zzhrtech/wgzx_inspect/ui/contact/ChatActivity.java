package com.zzhrtech.wgzx_inspect.ui.contact;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tencent.TIMCallBack;
import com.tencent.TIMConversationType;
import com.tencent.TIMElem;
import com.tencent.TIMElemType;
import com.tencent.TIMFaceElem;
import com.tencent.TIMImage;
import com.tencent.TIMImageElem;
import com.tencent.TIMImageType;
import com.tencent.TIMMessage;
import com.tencent.TIMMessageDraft;
import com.tencent.TIMSnapshot;
import com.tencent.TIMSoundElem;
import com.tencent.TIMTextElem;
import com.tencent.TIMValueCallBack;
import com.tencent.TIMVideo;
import com.tencent.TIMVideoElem;
import com.tencent.qcloud.presentation.event.RefreshEvent;
import com.tencent.qcloud.presentation.presenter.ChatPresenter;
import com.tencent.qcloud.presentation.viewfeatures.ChatView;
import com.tencent.qcloud.ui.ChatInput;
import com.tencent.qcloud.ui.VoiceSendingView;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.contact.ChatListAdapter;
import com.zzhrtech.wgzx_inspect.beans.ChatListBean;
import com.zzhrtech.wgzx_inspect.ui.me.ChartActivity;
import com.zzhrtech.wgzx_inspect.utils.EmoticonUtil;
import com.zzhrtech.wgzx_inspect.utils.FileUtil;
import com.zzhrtech.wgzx_inspect.utils.MediaUtil;
import com.zzhrtech.wgzx_inspect.utils.RecorderUtil;
import com.zzhrtech.wgzx_inspect.utils.TimeUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatActivity extends BaseActivity implements ChatView {
    public static String TAG = "ChatActivity";

    private static final int IMAGE_STORE = 200;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int IMAGE_PREVIEW = 400;
    private Uri fileUri;

    private ChatPresenter presenter;
    private String identify,nickname;
    private ChatInput chatInput;
    private RecyclerView recy_chat;
    private ChatListAdapter listAdapter;
    private List<TIMMessage> timMessages = new ArrayList<TIMMessage>();

    private VoiceSendingView voiceSendingView;

    private RecorderUtil recorder = new RecorderUtil();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        toolbar.setNavigationIcon(R.mipmap.ic_close);
        identify = getIntent().getStringExtra("identify");
        nickname = getIntent().getStringExtra("nickname");

        if (getSupportActionBar() != null)
        getSupportActionBar().setTitle(nickname);

        presenter = new ChatPresenter(this,identify, TIMConversationType.C2C);

        voiceSendingView = (VoiceSendingView) findViewById(R.id.voice_sending);

        chatInput = (ChatInput) findViewById(R.id.chart_input);
        chatInput.setChatView(this);

        recy_chat = (RecyclerView) findViewById(R.id.recy_chat);
        recy_chat.setLayoutManager(new LinearLayoutManager(this));
        recy_chat.setItemAnimator(new DefaultItemAnimator());

        recy_chat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                LinearLayoutManager layoutManager = (LinearLayoutManager) recy_chat.getLayoutManager();

                int firstItemPosition = layoutManager.findFirstVisibleItemPosition();
                switch (newState){
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        Picasso.with(ChatActivity.this).pauseTag(TAG);
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        Picasso.with(ChatActivity.this).resumeTag(TAG);

                        if (firstItemPosition == 0){
                            presenter.getMessage(listAdapter.getItemCount() > 0 ? timMessages.get(0) : null);
                        }
                        break;
                }

                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                super.onScrolled(recyclerView, dx, dy);
            }
        });

        listAdapter = new ChatListAdapter();
        recy_chat.setAdapter(listAdapter);

        presenter.start();
//
    }

    //显示消息
    @Override
    public void showMessage(TIMMessage message) {

        if (message == null){
            for (int i = 0;i< timMessages.size();i++){
                final int finalI = i;
                if (timMessages.get(i).getElement(0).getType() == TIMElemType.Image){
                    TIMImageElem imageElem = (TIMImageElem) timMessages.get(i).getElement(0);
                    for (final TIMImage image : imageElem.getImageList()) {
                        if (image.getType() == TIMImageType.Original) {
                            image.getImage(FileUtil.getCacheFilePath(image.getUuid()), new TIMCallBack() {
                                @Override
                                public void onError(int i, String s) {
                                }
                                @Override
                                public void onSuccess() {
                                    listAdapter.getbean(finalI).setImageOrigin(image.getUuid());
                                    listAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }

                }else if (timMessages.get(i).getElement(0).getType() == TIMElemType.Sound){
                    TIMSoundElem soundElem = (TIMSoundElem) timMessages.get(i).getElement(0);
                    soundElem.getSound(new TIMValueCallBack<byte[]>() {
                        @Override
                        public void onError(int i, String s) {

                        }

                        @Override
                        public void onSuccess(byte[] bytes) {
                            listAdapter.getbean(finalI).setVoice(bytes);
                            listAdapter.notifyDataSetChanged();
                        }
                    });
                }

            }
            listAdapter.notifyDataSetChanged();
        }else {
            if (message.getElement(0).getType() != TIMElemType.Custom) {
                final ChatListBean bean = new ChatListBean();
                if (message.isSelf()) {
                    bean.setSelf(true);
                } else {
                    bean.setSelf(false);
                }

                bean.setTime(TimeUtil.getTimeStr(message.timestamp()));

                SpannableStringBuilder text = new SpannableStringBuilder();
//            List<TIMElem> elems = new ArrayList<TIMElem>();
                for (int a = 0; a < message.getElementCount(); a++) {
                    if (message.getElement(a).getType() == TIMElemType.Text) {
                        TIMTextElem textElem = (TIMTextElem) message.getElement(a);
                        text.append(textElem.getText());
                        bean.setType(TIMElemType.Text);
                    } else if (message.getElement(a).getType() == TIMElemType.Face) {
                        bean.setType(TIMElemType.Face);
                        TIMFaceElem faceElem = (TIMFaceElem) message.getElement(a);
                        int startIndex = text.length();
                        try {
                            AssetManager am = this.getAssets();
                            InputStream is = am.open(String.format("emoticon/%d.gif", faceElem.getIndex()));
                            if (is == null) continue;
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            Matrix matrix = new Matrix();
                            int width = bitmap.getWidth();
                            int height = bitmap.getHeight();
                            matrix.postScale(2, 2);
                            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                                    width, height, matrix, true);
                            ImageSpan span = new ImageSpan(this, resizedBitmap, ImageSpan.ALIGN_BASELINE);
                            text.append(String.valueOf(faceElem.getIndex()));
                            text.setSpan(span, startIndex, startIndex + getNumLength(faceElem.getIndex()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            is.close();
                        } catch (IOException e) {

                        }
                    } else if (message.getElement(a).getType() == TIMElemType.Image) {

                        TIMImageElem imageElem = (TIMImageElem) message.getElement(a);

                        bean.setType(TIMElemType.Image);
                        bean.setImage(imageElem.getPath());

//                        mHandler.sendEmptyMessage(0);


                        for (final TIMImage image : imageElem.getImageList()) {

                            if (image.getType() == TIMImageType.Thumb) {
                                bean.setImage(image.getUrl());
                            }

                            if (image.getType() == TIMImageType.Original) {
                                image.getImage(FileUtil.getCacheFilePath(image.getUuid()), new TIMCallBack() {
                                    @Override
                                    public void onError(int i, String s) {

                                    }

                                    @Override
                                    public void onSuccess() {
                                        bean.setImageOrigin(image.getUuid());
                                        listAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                    } else if (message.getElement(a).getType() == TIMElemType.Video) {
                        TIMVideoElem videoElem = (TIMVideoElem) message.getElement(a);
                        final TIMSnapshot snapshot = videoElem.getSnapshotInfo();
                        bean.setType(TIMElemType.Video);
                        bean.setImage(videoElem.getSnapshotPath());
                        bean.setVideoPath(videoElem.getVideoPath());
                        snapshot.getImage(FileUtil.getCacheFilePath(snapshot.getUuid()), new TIMCallBack() {
                            @Override
                            public void onError(int i, String s) {

                            }

                            @Override
                            public void onSuccess() {

                                bean.setImage(FileUtil.getCacheFilePath(snapshot.getUuid()));
                                listAdapter.notifyDataSetChanged();
                            }
                        });

                        final TIMVideo video = videoElem.getVideoInfo();
                        video.getVideo(FileUtil.getCacheFilePath(video.getUuid()), new TIMCallBack() {
                            @Override
                            public void onError(int i, String s) {

                            }

                            @Override
                            public void onSuccess() {
                                bean.setVideoPath(FileUtil.getCacheFilePath(video.getUuid()));
                                listAdapter.notifyDataSetChanged();
                            }
                        });

                    } else if (message.getElement(a).getType() == TIMElemType.Sound) {
                        TIMSoundElem soundElem = (TIMSoundElem) message.getElement(a);
                        soundElem.getSound(new TIMValueCallBack<byte[]>() {
                            @Override
                            public void onError(int i, String s) {

                            }

                            @Override
                            public void onSuccess(byte[] bytes) {
                                bean.setVoice(bytes);
                                listAdapter.notifyDataSetChanged();

                            }
                        });

                        text.append(soundElem.getDuration() + "");
                        //表示有语音
                        bean.setSoundimg(1);
                        bean.setType(TIMElemType.Sound);
                    }
                }

                bean.setText(text);
                listAdapter.addItem(bean);

                //滑动用
                timMessages.add(message);
                recy_chat.scrollToPosition(listAdapter.getItemCount() - 1);
            }
        }
    }

    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {



            listAdapter.notifyDataSetChanged();

            return false;
        }
    });

    @Override
    public void showMessage(List<TIMMessage> messages) {

        int newMsgNum = 0;

        for (int i = 0; i < messages.size();i++){
            TIMMessage message = messages.get(i);
            final ChatListBean bean = new ChatListBean();
            if (message.isSelf()){
                bean.setSelf(true);
            }else {
                bean.setSelf(false);
            }

            bean.setTime(TimeUtil.getTimeStr(message.timestamp()));

            SpannableStringBuilder text = new SpannableStringBuilder();
//            List<TIMElem> elems = new ArrayList<TIMElem>();
            for (int a = 0;a < message.getElementCount();a++) {
                if (message.getElement(a).getType() == TIMElemType.Text){
                    TIMTextElem textElem = (TIMTextElem) message.getElement(a);
                    text.append(textElem.getText());
                    bean.setType(TIMElemType.Text);
                }else if (message.getElement(a).getType() == TIMElemType.Face){
                    bean.setType(TIMElemType.Face);
                    TIMFaceElem faceElem = (TIMFaceElem) message.getElement(a);
                    int startIndex = text.length();
                    try{
                        AssetManager am = this.getAssets();
                        InputStream is = am.open(String.format("emoticon/%d.gif", faceElem.getIndex()));
                        if (is == null) continue;
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        Matrix matrix = new Matrix();
                        int width = bitmap.getWidth();
                        int height = bitmap.getHeight();
                        matrix.postScale(2, 2);
                        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                                width, height, matrix, true);
                        ImageSpan span = new ImageSpan(this, resizedBitmap, ImageSpan.ALIGN_BASELINE);
                        text.append(String.valueOf(faceElem.getIndex()));
                        text.setSpan(span, startIndex, startIndex + getNumLength(faceElem.getIndex()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        is.close();
                    }catch (IOException e){

                    }
                }else if (message.getElement(a).getType() == TIMElemType.Image){

                    TIMImageElem imageElem = (TIMImageElem) message.getElement(a);


                    for (final TIMImage image : imageElem.getImageList()){

                        bean.setType(TIMElemType.Image);

                        if (image.getType() == TIMImageType.Thumb){

                            if (FileUtil.isCacheFileExist(image.getUuid())){
                                bean.setImage(FileUtil.getCacheFilePath(image.getUuid()));
                            }
                            else {
                                image.getImage(FileUtil.getCacheFilePath(image.getUuid()), new TIMCallBack() {
                                    @Override
                                    public void onError(int i, String s) {

                                    }

                                    @Override
                                    public void onSuccess() {

                                        bean.setImage(FileUtil.getCacheFilePath(image.getUuid()));
                                        listAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
//                            bean.setImage(image.getUrl());
//                            bean.setType(TIMElemType.Image);
                        }
                        if (image.getType() == TIMImageType.Original){
                            if (FileUtil.isCacheFileExist(image.getUuid())){
                                bean.setImageOrigin(image.getUuid());
                            }else {
                                image.getImage(FileUtil.getCacheFilePath(image.getUuid()), new TIMCallBack() {
                                    @Override
                                    public void onError(int i, String s) {

                                    }

                                    @Override
                                    public void onSuccess() {
                                        bean.setImageOrigin(image.getUuid());
                                        listAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                    }
                }else if (message.getElement(a).getType() == TIMElemType.Video){
                    TIMVideoElem videoElem = (TIMVideoElem) message.getElement(a);
                    final TIMSnapshot snapshot = videoElem.getSnapshotInfo();
                    bean.setType(TIMElemType.Video);

                    if (FileUtil.isCacheFileExist(snapshot.getUuid())){
                        bean.setImage(FileUtil.getCacheFilePath(snapshot.getUuid()));
                    }else {

                        snapshot.getImage(FileUtil.getCacheFilePath(snapshot.getUuid()), new TIMCallBack() {
                            @Override
                            public void onError(int i, String s) {

                            }

                            @Override
                            public void onSuccess() {

                                bean.setImage(FileUtil.getCacheFilePath(snapshot.getUuid()));
                                listAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    final TIMVideo video = videoElem.getVideoInfo();

                    if (FileUtil.isCacheFileExist(video.getUuid())){
                        bean.setVideoPath(FileUtil.getCacheFilePath(video.getUuid()));
                    }else {
                        video.getVideo(FileUtil.getCacheFilePath(video.getUuid()), new TIMCallBack() {
                            @Override
                            public void onError(int i, String s) {

                            }

                            @Override
                            public void onSuccess() {
                                bean.setVideoPath(FileUtil.getCacheFilePath(video.getUuid()));
                                listAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                }else if (message.getElement(a).getType() == TIMElemType.Sound){
                    TIMSoundElem soundElem = (TIMSoundElem) message.getElement(a);
                    soundElem.getSound(new TIMValueCallBack<byte[]>() {
                        @Override
                        public void onError(int i, String s) {

                        }

                        @Override
                        public void onSuccess(byte[] bytes) {

                            bean.setVoice(bytes);
                            listAdapter.notifyDataSetChanged();

                        }
                    });

                    text.append(soundElem.getDuration() + "");
                    //表示有语音
                    bean.setSoundimg(1);
                    bean.setType(TIMElemType.Sound);
                }
            }

            bean.setText(text);
            listAdapter.initaddItem(bean);
            //滑动用
            timMessages.add(0,message);
            ++newMsgNum;
        }

        recy_chat.scrollToPosition(newMsgNum - 1);

    }

    private int getNumLength(int n){
        return String.valueOf(n).length();
    }

    //清除所有消息
    @Override
    public void clearAllMessage() {

    }

    //消息发送成功
    @Override
    public void onSendMessageSuccess(TIMMessage message) {

        showMessage(message);
    }

    //发送失败
    @Override
    public void onSendMessageFail(int code, String desc, TIMMessage message) {


    }

    //发送图片
    @Override
    public void sendImage() {
        Intent intent_album = new Intent("android.intent.action.GET_CONTENT");
        intent_album.setType("image/*");
        startActivityForResult(intent_album, IMAGE_STORE);
    }

    //发送照片
    @Override
    public void sendPhoto() {

        Intent intent_photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent_photo.resolveActivity(getPackageManager()) != null) {
            File tempFile = FileUtil.getTempFile(FileUtil.FileType.IMG);
            if (tempFile != null) {
                fileUri = Uri.fromFile(tempFile);
            }
            intent_photo.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent_photo, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    //发文本
    @Override
    public void sendText() {
        Editable s = chatInput.getText();
        TIMMessage message = new TIMMessage();
        ImageSpan[] spans = s.getSpans(0, s.length(), ImageSpan.class);
        int currentIndex = 0;
        for (ImageSpan span : spans){
            int startIndex = s.getSpanStart(span);
            int endIndex = s.getSpanEnd(span);
            if (currentIndex < startIndex){
                TIMTextElem textElem = new TIMTextElem();
                textElem.setText(s.subSequence(currentIndex, startIndex).toString());
                message.addElement(textElem);
            }
            TIMFaceElem faceElem = new TIMFaceElem();
            int index = Integer.parseInt(s.subSequence(startIndex, endIndex).toString());
            faceElem.setIndex(index);
            if (index < EmoticonUtil.emoticonData.length){
                faceElem.setData(EmoticonUtil.emoticonData[index].getBytes(Charset.forName("UTF-8")));
            }
            message.addElement(faceElem);
            currentIndex = endIndex;
        }
        if (currentIndex < s.length()){
            TIMTextElem textElem = new TIMTextElem();
            textElem.setText(s.subSequence(currentIndex, s.length()).toString());
            message.addElement(textElem);
        }

        presenter.sendMessage(message);
        chatInput.setText("");
    }


    //发文件
    @Override
    public void sendFile() {

    }

    //发语音
    @Override
    public void startSendVoice() {
        voiceSendingView.setVisibility(View.VISIBLE);
        voiceSendingView.showRecording();
        recorder.startRecording();
    }

    @Override
    public void endSendVoice() {
        voiceSendingView.release();
        voiceSendingView.setVisibility(View.GONE);
        recorder.stopRecording();
        if (recorder.getTimeInterval() < 1) {
            Toast.makeText(this, getResources().getString(R.string.chat_audio_too_short), Toast.LENGTH_SHORT).show();
        } else {
            TIMMessage message = new TIMMessage();
            TIMSoundElem elem = new TIMSoundElem();
            elem.setPath(recorder.getFilePath());
            elem.setDuration(recorder.getTimeInterval());  //填写语音时长
            message.addElement(elem);
            presenter.sendMessage(message);
        }
    }

    //发小视频
    @Override
    public void sendVideo(String fileName) {

        TIMMessage message = new TIMMessage();
        TIMVideoElem elem = new TIMVideoElem();
        elem.setVideoPath(FileUtil.getCacheFilePath(fileName));
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(FileUtil.getCacheFilePath(fileName), MediaStore.Images.Thumbnails.MINI_KIND);
        elem.setSnapshotPath(FileUtil.createFile(thumb, new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())));
        TIMSnapshot snapshot = new TIMSnapshot();
        snapshot.setType("PNG");
        snapshot.setHeight(thumb.getHeight());
        snapshot.setWidth(thumb.getWidth());
        TIMVideo video = new TIMVideo();
        video.setType("MP4");
        video.setDuaration(MediaUtil.getInstance().getDuration(FileUtil.getCacheFilePath(fileName)));
        elem.setSnapshot(snapshot);
        elem.setVideo(video);
        message.addElement(elem);

        presenter.sendMessage(message);
    }

    @Override
    public void cancelSendVoice() {

    }

    //正在发送
    @Override
    public void sending() {

    }

    //草稿
    @Override
    public void showDraft(TIMMessageDraft draft) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && fileUri != null) {
                showImagePreview(fileUri.getPath());
            }
        } else if (requestCode == IMAGE_STORE) {
            if (resultCode == RESULT_OK && data != null) {
                showImagePreview(FileUtil.getImageFilePath(this, data.getData()));
            }

        }else if (requestCode == IMAGE_PREVIEW){
            if (resultCode == RESULT_OK) {
                boolean isOri = data.getBooleanExtra("isOri",false);
                String path = data.getStringExtra("path");
                File file = new File(path);
                if (file.exists() && file.length() > 0){
                    if (file.length() > 1024 * 1024 * 10){
                        Toast.makeText(this, getString(R.string.chat_file_too_large),Toast.LENGTH_SHORT).show();
                    }else{
                        TIMMessage message = new TIMMessage();
                        TIMImageElem elem = new TIMImageElem();
                        elem.setPath(path);
                        elem.setLevel(isOri?0:1);
                        message.addElement(elem);
                        presenter.sendMessage(message);
                    }
                }else{
                    Toast.makeText(this, getString(R.string.chat_file_not_exist),Toast.LENGTH_SHORT).show();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void showImagePreview(String path){
        if (path == null) return;
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra("path", path);
        startActivityForResult(intent, IMAGE_PREVIEW);
    }
}
