package com.zzhrtech.wgzx_inspect.ui.contact;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tencent.TIMConversationType;
import com.tencent.TIMFriendshipProxy;
import com.tencent.TIMUserProfile;
import com.tencent.qcloud.presentation.event.FriendshipEvent;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.contact.ContactallListAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.ConversationBean;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactallFragment extends BaseFragment implements Observer {

    private RecyclerView recy_cont;
    private ContactallListAdapter listAdapter;

    public ContactallFragment() {
        // Required empty public constructor
    }

    public static ContactallFragment newInstance() {
        
        Bundle args = new Bundle();
        
        ContactallFragment fragment = new ContactallFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_contactall, container, false);

        recy_cont = (RecyclerView) view.findViewById(R.id.recy_cont);
        recy_cont.setLayoutManager(new LinearLayoutManager(mActivity));
        recy_cont.setItemAnimator(new DefaultItemAnimator());

        listAdapter = new ContactallListAdapter();
        recy_cont.setAdapter(listAdapter);

        listAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                com.zzhrtech.wgzx_inspect.ui.chat.ChatActivity.navToChat(mActivity,listAdapter.getIden(position), TIMConversationType.C2C);


//                Intent intent = new Intent(mActivity, ChatActivity.class);
//                intent.putExtra("identify",listAdapter.getIden(position));
//                intent.putExtra("nickname",listAdapter.getNickname(position));
//                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    protected void initData() {
        super.initData();

        FriendshipEvent.getInstance().addObserver(this);
        List<TIMUserProfile> allfriends = TIMFriendshipProxy.getInstance().getFriends();
        for (TIMUserProfile profile : allfriends){
            ConversationBean bean = new ConversationBean();
            bean.setIdentifier(profile.getIdentifier());
            bean.setNickname(profile.getNickName());
            bean.setHead(profile.getFaceUrl());
            listAdapter.addItem(bean);
        }

    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof FriendshipEvent){
            if (data instanceof FriendshipEvent.NotifyCmd){
                FriendshipEvent.NotifyCmd cmd = (FriendshipEvent.NotifyCmd) data;
                switch (cmd.type){
                    case REFRESH:
                    case DEL:
                    case ADD:
                    case PROFILE_UPDATE:
                    case ADD_REQ:
                    case GROUP_UPDATE:

                        if (listAdapter.getItemCount() == 0){
                        List<TIMUserProfile> allfriends = TIMFriendshipProxy.getInstance().getFriends();
                        for (TIMUserProfile profile : allfriends) {
                            ConversationBean bean = new ConversationBean();
                            bean.setIdentifier(profile.getIdentifier());
                            bean.setNickname(profile.getNickName());
                            bean.setHead(profile.getFaceUrl());
                            listAdapter.addItem(bean);
                        }
                        }

                        break;

                }
            }
        }
    }
}
