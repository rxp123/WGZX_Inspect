package com.zzhrtech.wgzx_inspect.ui.contact;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tencent.TIMConversation;
import com.tencent.TIMConversationType;
import com.tencent.TIMElem;
import com.tencent.TIMElemType;
import com.tencent.TIMFriendFutureItem;
import com.tencent.TIMFriendshipProxy;
import com.tencent.TIMGroupCacheInfo;
import com.tencent.TIMManager;
import com.tencent.TIMMessage;
import com.tencent.TIMTextElem;
import com.tencent.TIMUserProfile;
import com.tencent.qcloud.presentation.presenter.ChatPresenter;
import com.tencent.qcloud.presentation.presenter.ConversationPresenter;
import com.tencent.qcloud.presentation.presenter.FriendshipManagerPresenter;
import com.tencent.qcloud.presentation.viewfeatures.ConversationView;
import com.tencent.qcloud.presentation.viewfeatures.FriendshipMessageView;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.contact.ConverstionListAdapter;
import com.zzhrtech.wgzx_inspect.base.BaseFragment;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.ConversationBean;
import com.zzhrtech.wgzx_inspect.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConversationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConversationFragment extends BaseFragment implements ConversationView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ConversationPresenter presenter;
    private List<ConversationBean> beanList;

    TextView emptyText;

    private RecyclerView recy_conver;
    private ConverstionListAdapter listAdapter;


    public ConversationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConversationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConversationFragment newInstance(String param1, String param2) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View view =  inflater.inflate(R.layout.fragment_conversation, container, false);
        emptyText = (TextView) view.findViewById(R.id.emptyText);
        recy_conver = (RecyclerView) view.findViewById(R.id.recy_conver);
        recy_conver.setLayoutManager(new LinearLayoutManager(mActivity));
        recy_conver.setItemAnimator(new DefaultItemAnimator());
        listAdapter = new ConverstionListAdapter();
        recy_conver.setAdapter(listAdapter);

        listAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {

                com.zzhrtech.wgzx_inspect.ui.chat.ChatActivity.navToChat(mActivity,listAdapter.getBeanList().get(position).getIdentifier(),TIMConversationType.C2C);
//
//                Intent intent = new Intent(mActivity, ChatActivity.class);
//                intent.putExtra("identify",listAdapter.getBeanList().get(position).getIdentifier());
//                intent.putExtra("nickname",listAdapter.getBeanList().get(position).getNickname());
//                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    protected void initData() {
        super.initData();

        presenter = new ConversationPresenter(this);
        presenter.getConversation();
    }

    @Override
    public void initView(List<TIMConversation> conversationList) {
//        beanList = new ArrayList<ConversationBean>();
        List<String> ids = new ArrayList<String>();

        for (int i = 0;i<conversationList.size();i++){
            ids.add(conversationList.get(i).getPeer());
        }

        List<TIMUserProfile> userProfiles = TIMFriendshipProxy.getInstance().getFriendsById(ids);

        if (userProfiles.size() > 0) {
            emptyText.setVisibility(View.GONE);
            for (TIMUserProfile profile : userProfiles) {
                ConversationBean bean = new ConversationBean();
                bean.setIdentifier(profile.getIdentifier());
                bean.setNickname(profile.getNickName());
                bean.setHead(profile.getFaceUrl());
                listAdapter.addItem(bean);
            }
        }else {
            emptyText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateMessage(TIMMessage message) {

        if (message == null){
            listAdapter.notifyDataSetChanged();
            return;
        }

        if (message.getConversation().getType() == TIMConversationType.System)
            return;

        if (message.getElement(0).getType() == TIMElemType.Custom)
            return;

         if (message.getElementCount() > 0) {
                //...处理更多消息

                String friendIden = message.getConversation().getPeer();

                for (int i = 0; i < listAdapter.getBeanList().size(); i++) {

                    if (listAdapter.getBeanList().get(i).getIdentifier().equals(friendIden)) {

                        listAdapter.getBeanList().remove(i);

                        break;
                    }
                }

                List<String> id = new ArrayList<String>();
                id.add(friendIden);
                List<TIMUserProfile> userProfile = TIMFriendshipProxy.getInstance().getFriendsById(id);
                if (userProfile.size() > 0) {

                    emptyText.setVisibility(View.GONE);

                    ConversationBean bean = new ConversationBean();
                    bean.setIdentifier(userProfile.get(0).getIdentifier());
                    bean.setNickname(userProfile.get(0).getNickName());
                    bean.setHead(userProfile.get(0).getFaceUrl());
                    listAdapter.addItem(bean);
                }


                TIMElem elem = message.getElement(0);
                TIMElemType elemType = elem.getType();
                if (elemType == TIMElemType.Text) {
                    TIMTextElem textElem = (TIMTextElem) elem;
                    listAdapter.addlastMessage(message.getConversation().getPeer(), textElem.getText(), TimeUtil.getTimeStr(message.timestamp()), message.getConversation().getUnreadMessageNum());
                    //处理文本消息
                } else if (elemType == TIMElemType.Image) {
                    listAdapter.addlastMessage(message.getConversation().getPeer(), "[图片]", TimeUtil.getTimeStr(message.timestamp()), message.getConversation().getUnreadMessageNum());
                    //处理图片消息
                } else if (elemType == TIMElemType.Sound) {
                    listAdapter.addlastMessage(message.getConversation().getPeer(), "[语音]", TimeUtil.getTimeStr(message.timestamp()), message.getConversation().getUnreadMessageNum());
                } else if (elemType == TIMElemType.Video) {
                    listAdapter.addlastMessage(message.getConversation().getPeer(), "[视频]", TimeUtil.getTimeStr(message.timestamp()), message.getConversation().getUnreadMessageNum());
                }

                refresh();
            }



    }

    @Override
    public void onResume() {
        super.onResume();
        listAdapter.notifyDataSetChanged();
        refresh();
        PushUtil.getInstance().reset();
    }

    @Override
    public void updateFriendshipMessage() {

    }

    @Override
    public void removeConversation(String identify) {

    }

    @Override
    public void updateGroupInfo(TIMGroupCacheInfo info) {

    }

    @Override
    public void refresh() {

        listAdapter.notifyDataSetChanged();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contact, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.contact){
            Intent intent = new Intent(mActivity,ContactAllActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
