package com.zzhrtech.wgzx_inspect.ui.disease;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.disease.DiseaseTypeAdapter;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.disease.DiseaseTypeBean;

import java.util.ArrayList;
import java.util.List;

public class DiseaseTypeActivity extends BaseActivity {
    private RecyclerView rv_disease;
    private DiseaseTypeAdapter typeAdapter;
    private List<DiseaseTypeBean> typeBeanList;
    private String[] names = {"道路","桥梁","排水","停车","防汛","电话与微博"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease_type);
        getData();
        initView();
    }

    private void getData(){
        typeBeanList = new ArrayList<DiseaseTypeBean>();
        for (int i = 0;i < names.length;i++){
            DiseaseTypeBean bean = new DiseaseTypeBean();
            bean.setName(names[i]);
            typeBeanList.add(bean);
        }
    }

    private void initView(){
        rv_disease = (RecyclerView) findViewById(R.id.rv_disease);
        rv_disease.setLayoutManager(new GridLayoutManager(this,2));
        rv_disease.setItemAnimator(new DefaultItemAnimator());
        typeAdapter = new DiseaseTypeAdapter();
        rv_disease.setAdapter(typeAdapter);

//        for (DiseaseTypeBean bean : typeBeanList){
//            typeAdapter.addItem(bean);
//        }

        typeAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {

                Intent intent = new Intent(DiseaseTypeActivity.this,DiseaseUploadActivity.class);
                startActivity(intent);

            }
        });
    }
}
