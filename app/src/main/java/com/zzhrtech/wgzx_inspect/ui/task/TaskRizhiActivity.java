package com.zzhrtech.wgzx_inspect.ui.task;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskFilterAdapter;
import com.zzhrtech.wgzx_inspect.adapters.task.TaskListAdapter;
import com.zzhrtech.wgzx_inspect.beans.task.TaskFilterBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRizhiActivity extends BaseActivity {

    RecyclerView recy_rizhi;
    TaskFilterAdapter filterAdapter;
    String taskId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_rizhi);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        taskId = getIntent().getStringExtra("taskid");
        initView();
        httpGetRizhi(taskId);
    }

    private void initView(){

        recy_rizhi = (RecyclerView) findViewById(R.id.recy_rizhi);

        recy_rizhi.setLayoutManager(new LinearLayoutManager(this));
        recy_rizhi.setItemAnimator(new DefaultItemAnimator());

        filterAdapter = new TaskFilterAdapter();
        recy_rizhi.setAdapter(filterAdapter);

    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            List<TaskFilterBean.Data> dataList = (List<TaskFilterBean.Data>) msg.obj;
            for (TaskFilterBean.Data data : dataList){
                filterAdapter.addItem(data);
            }

            return false;
        }
    });


    public void httpGetRizhi(String taskid){

        Map<String,String> params = new HashMap<String, String>();
        params.put("task_id",taskid);


        GsonRequest<TaskFilterBean> gsonRequest = new GsonRequest<TaskFilterBean>(com.android.volley.Request.Method.POST, Constans.HTTP_TASKLOG, TaskFilterBean.class, params, new com.android.volley.Response.Listener<TaskFilterBean>() {
            @Override
            public void onResponse(TaskFilterBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {


                    AppLogger.d(response.getMsg());
//                    showToast(tv_time,response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

}
