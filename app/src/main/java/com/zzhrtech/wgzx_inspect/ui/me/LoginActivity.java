package com.zzhrtech.wgzx_inspect.ui.me;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
//import com.hyphenate.EMCallBack;
//import com.hyphenate.chat.EMClient;
import com.tencent.TIMCallBack;
import com.tencent.TIMManager;
import com.tencent.TIMUser;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.qcloud.presentation.Constant;
import com.tencent.qcloud.presentation.business.LoginBusiness;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.disease.CateListBean;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.beans.task.DepatmentBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.ui.contact.PushUtil;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements TIMCallBack {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;


    private MaterialDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolbar.setNavigationIcon(R.mipmap.ic_close);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }




    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.

        if(!isPhoneNumberValid(email)){
            mEmailView.setError("手机号格式不正确");
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("手机号不能为空");
            focusView = mEmailView;
            cancel = true;
        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            progress = new MaterialDialog.Builder(LoginActivity.this)
                    .title("请等待")
                    .content("正在登录")
                    .progress(true,0)
                    .show();

            httpLogin(email,password);
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private void httpLogin(String uname,String pwd){
        Map<String,String> params = new HashMap<String, String>();
        params.put("username",uname);
        params.put("password",pwd);

        GsonRequest<LoginBean> gsonRequest = new GsonRequest<LoginBean>(com.android.volley.Request.Method.POST, Constans.HTTP_LOGIN, LoginBean.class, params, new com.android.volley.Response.Listener<LoginBean>() {
            @Override
            public void onResponse(LoginBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    if (response.getCode().equals("00003")){
                        showToast("该人员不存在");
                    }else {
                        showToast(response.getMsg());
                    }
                    progress.dismiss();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
                progress.dismiss();
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    private void httpgetDep(String depId){
        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",depId);

        GsonRequest<DepatmentBean> gsonRequest = new GsonRequest<DepatmentBean>(com.android.volley.Request.Method.POST, Constans.HTTP_GETDEPART, DepatmentBean.class, params, new com.android.volley.Response.Listener<DepatmentBean>() {
            @Override
            public void onResponse(DepatmentBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 1;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                    progress.dismiss();

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
//                mDialog.dismiss();
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }


    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            if (msg.what == 0) {

                LoginBean.Data data = (LoginBean.Data) msg.obj;
                getCache().put(keyUid, data.getUid());
                getCache().put(keyUsername, data.getUsername());
                getCache().put(keyNickname, data.getNickname());
                getCache().put(keyHead, data.getViewhead());
                getCache().put(keyDepid, data.getDep_id());
                getCache().put(keyState,data.getState());
                getCache().put(keyPid,data.getPid());
                getCache().put(keyPassword,mPasswordView.getText().toString());
                getCache().put(keySig,data.getSig());
                getCache().put(keyIsUserboss,data.getIsboss());

                httpgetDep(data.getDep_id());

            }else {
                DepatmentBean.Data data = (DepatmentBean.Data) msg.obj;
                getCache().put(keyIsboss,data.getIsboss());

                loginim();
            }
            return false;
        }
    });


    private void loginim(){
        //登陆
        LoginBusiness.loginIm(getCache().getAsString(keyUsername), getCache().getAsString(keySig), this);
    }

    @Override
    public void onError(int i, String s) {

        progress.dismiss();

        switch (i) {
            case 6208:
//                new AlertDialog.Builder(LoginActivity.this)
//                        .setMessage("你的账号已在其他终端登录,重新登录")
//                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                finish();
//                            }
//                        })
//                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
                                loginim();
//                            }
//                        }).create().show();
                break;
        }
    }

    @Override
    public void onSuccess() {

        progress.dismiss();
        PushUtil.getInstance();
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }


    /*检查字符串是否为电话号码的方法,并返回true or false的判断值*/
    public static boolean isPhoneNumberValid(String phoneNumber)
    {
        boolean isValid = false;
    /* 可接受的电话格式有:
     * ^\\(? : 可以使用 "(" 作为开头
     * (\\d{3}): 紧接着三个数字
     * \\)? : 可以使用")"接续
     * [- ]? : 在上述格式后可以使用具选择性的 "-".
     * (\\d{4}) : 再紧接着三个数字
     * [- ]? : 可以使用具选择性的 "-" 接续.
     * (\\d{4})$: 以四个数字结束.
     * 可以比较下列数字格式:
     * (123)456-78900, 123-4560-7890, 12345678900, (123)-4560-7890
    */
        String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{5})$";
        String expression2 ="^\\(?(\\d{3})\\)?[- ]?(\\d{4})[- ]?(\\d{4})$";
        CharSequence inputStr = phoneNumber;
    /*创建Pattern*/
        Pattern pattern = Pattern.compile(expression);
    /*将Pattern 以参数传入Matcher作Regular expression*/
        Matcher matcher = pattern.matcher(inputStr);
    /*创建Pattern2*/
        Pattern pattern2 =Pattern.compile(expression2);
    /*将Pattern2 以参数传入Matcher2作Regular expression*/
        Matcher matcher2= pattern2.matcher(inputStr);
        if(matcher.matches()||matcher2.matches())
        {
            isValid = true;
        }
        return isValid;
    }
}

