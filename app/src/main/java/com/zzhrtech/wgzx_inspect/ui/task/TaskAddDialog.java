package com.zzhrtech.wgzx_inspect.ui.task;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.disease.CateContentBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateInfoBean;
import com.zzhrtech.wgzx_inspect.beans.disease.CateListBean;
import com.zzhrtech.wgzx_inspect.beans.task.UnitBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;
import com.zzhrtech.wgzx_inspect.view.CustomToast;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by renxiangpeng on 2017/1/9.
 */

public class TaskAddDialog extends Dialog {

    String mdepId;
    Context mContext;

    NiceSpinner spinner_type,spinner_class,spinner_content,spinner_arceage;
    EditText et_arec;

    Button btn_sure,btn_cancle;
    private List<CateListBean.Data> cateList;
    private List<CateInfoBean.Data> cateInfoList;
    private List<CateContentBean.Data> cateContentList;
    private List<UnitBean.Data> unitBeanList;

    private String mcatId = "",mciId = "",mcisId = "",munitId = "";
    private String mcatName = "",mciName = "",mcisName = "",munitName = "";


    private OndataListener mOndataListener;


    public TaskAddDialog(Context context,String depid,OndataListener ondataListener) {
        super(context,R.style.dialog);
        mContext = context;
        mOndataListener = ondataListener;
        mdepId = depid;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.layout_addarceage, null);
        this.setContentView(layout);


        spinner_type = (NiceSpinner) layout.findViewById(R.id.spinner_type);
        spinner_class = (NiceSpinner) layout.findViewById(R.id.spinner_class);
        spinner_content = (NiceSpinner) layout.findViewById(R.id.spinner_content);
        spinner_arceage = (NiceSpinner) layout.findViewById(R.id.spinner_arceage);
        btn_sure = (Button) layout.findViewById(R.id.btn_sure);
        btn_cancle = (Button) layout.findViewById(R.id.btn_cancle);
        et_arec = (EditText) layout.findViewById(R.id.et_arec);
        spinner_type.setOnItemSelectedListener(spinnerListener);
        spinner_class.setOnItemSelectedListener(spinnerListener);
        spinner_content.setOnItemSelectedListener(spinnerListener);
        spinner_arceage.setOnItemSelectedListener(spinnerListener);

        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mcatId.equals("")){
                    CustomToast.showToast(mContext,"请选择类型！",2000);
                }else if (mciId.equals("")){
                    CustomToast.showToast(mContext,"请选择种类！",2000);
                }else if (mcisId.equals("")){
                    CustomToast.showToast(mContext,"请选择内容！",2000);
                }else if (munitId.equals("")){
                    CustomToast.showToast(mContext,"请选择单位！",2000);
                }
                else {
                    mOndataListener.onData(mcatName,mcatId,mciName,mciId,mcisName,mcisId,et_arec.getText().toString(),munitName,munitId);

                    dismiss();
                }
            }
        });

        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        httpGetCate(mdepId);
        httpGetUtil();
    }


    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (position > 0) {

                if (parent.getId() == R.id.spinner_type) {
                    mcatId = cateList.get(position-1).getCat_id();
                    mcatName = cateList.get(position - 1).getCat_name();
                    httpGetCateInfo(cateList.get(position-1).getCat_id());
                } else if (parent.getId() == R.id.spinner_class) {
                    mciId = cateInfoList.get(position-1).getCat_id();
                    mciName = cateInfoList.get(position - 1).getCat_name();
                    httpGetCateContent(mciId);
                } else if (parent.getId() == R.id.spinner_content){
                    mcisId = cateContentList.get(position-1).getCat_id();
                    mcisName = cateContentList.get(position - 1).getCat_name();
                } else if (parent.getId() == R.id.spinner_arceage){
                    munitId = unitBeanList.get(position-1).getId();
                    munitName = unitBeanList.get(position - 1).getName();
                }
            }else {

                if (parent.getId() == R.id.spinner_type) {
                    mcatId = "";
                    mcatName = "";
                    spinner_class.attachDataSource(new ArrayList<String>());
                } else if (parent.getId() == R.id.spinner_class) {
                    mciId = "";
                    mciName = "";
                    spinner_content.attachDataSource(new ArrayList<String>());
                } else if (parent.getId() == R.id.spinner_content){
                    mcisId = "";
                    mcisName = "";
                } else if (parent.getId() == R.id.spinner_arceage){
                    munitId = "";
                    munitName = "";
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };




    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what){
                case 0:
                    cateList = (List<CateListBean.Data>) msg.obj;
                    List<String> dataset = new ArrayList<String>();
                    for (int i = 0;i<cateList.size();i++){
                        dataset.add(cateList.get(i).getCat_name());
                    }
                    spinner_type.attachDataSource(dataset);
                    break;
                case 1:
                    cateInfoList = (List<CateInfoBean.Data>) msg.obj;
                    dataset = new ArrayList<String>();
                    for (int i = 0;i<cateInfoList.size();i++){
                        dataset.add(cateInfoList.get(i).getCat_name());
                    }
                    spinner_class.attachDataSource(dataset);
                    break;
                case 2:

                    cateContentList = (List<CateContentBean.Data>) msg.obj;
                    dataset = new ArrayList<String>();
                    for (int i = 0;i<cateContentList.size();i++){
                        dataset.add(cateContentList.get(i).getCat_name());
                    }
                    spinner_content.attachDataSource(dataset);
                    break;
                case 4:
                    unitBeanList = (List<UnitBean.Data>) msg.obj;
                    dataset = new ArrayList<String>();
                    for (int i = 0;i<unitBeanList.size();i++){
                        dataset.add(unitBeanList.get(i).getName());
                    }
                    spinner_arceage.attachDataSource(dataset);
                    break;
            }

            return false;
        }
    });


    //获取类型
    private void httpGetCate(String roleid){

        Map<String,String> params = new HashMap<String, String>();
        params.put("dep_id",roleid);

        GsonRequest<CateListBean> gsonRequest = new GsonRequest<CateListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATELIST, CateListBean.class, params, new com.android.volley.Response.Listener<CateListBean>() {
            @Override
            public void onResponse(CateListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获取种类
    private void httpGetCateInfo(String catid){
        Map<String,String> params = new HashMap<String, String>();
        params.put("cat_id",catid);

        GsonRequest<CateInfoBean> gsonRequest = new GsonRequest<CateInfoBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATEINFO, CateInfoBean.class, params, new com.android.volley.Response.Listener<CateInfoBean>() {
            @Override
            public void onResponse(CateInfoBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 1;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg() + "");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    //获取内容
    private void httpGetCateContent(String ciid){
        Map<String,String> params = new HashMap<String, String>();
        params.put("ci_id",ciid);
        GsonRequest<CateContentBean> gsonRequest = new GsonRequest<CateContentBean>(com.android.volley.Request.Method.POST, Constans.HTTP_CATECONTENT, CateContentBean.class, params, new com.android.volley.Response.Listener<CateContentBean>() {
            @Override
            public void onResponse(CateContentBean response) {
                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 2;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg() + "");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpGetUtil(){

        GsonRequest<UnitBean> gsonRequest = new GsonRequest<UnitBean>(com.android.volley.Request.Method.POST, Constans.HTTP_UNITLIST, UnitBean.class, null, new com.android.volley.Response.Listener<UnitBean>() {
            @Override
            public void onResponse(UnitBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 4;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
//                    showToast(response.getMsg());
                    AppLogger.d(response.getMsg() + "");
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    public interface OndataListener{
        public void onData(String typestr,String typeid,String classstr,String classid,String contentstr,String contentid,String arec,String unitstr,String unitid);
    }
}
