package com.zzhrtech.wgzx_inspect.ui.task;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.task.BossListAdapter;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.task.BossListBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BossListActivity extends BaseActivity {

    private RecyclerView recy_boss;
    private BossListAdapter listAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boss_list);

        recy_boss = (RecyclerView) findViewById(R.id.recy_boss);
        recy_boss.setLayoutManager(new LinearLayoutManager(this));
        recy_boss.setItemAnimator(new DefaultItemAnimator());

        listAdapter = new BossListAdapter();
        recy_boss.setAdapter(listAdapter);

        listAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent();
                intent.putExtra("uid",listAdapter.getBossId(position));
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        httpGetBoss();
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            List<BossListBean.Data> dataList = (List<BossListBean.Data>) msg.obj;

            for (BossListBean.Data data : dataList){
                listAdapter.addItem(data);
            }

            return false;
        }
    });

    public void httpGetBoss(){

        Map<String,String> params = new HashMap<String, String>();
        params.put("uid", getCache().getAsString(Constans.keyUid));


        GsonRequest<BossListBean> gsonRequest = new GsonRequest<BossListBean>(com.android.volley.Request.Method.POST, Constans.HTTP_BOSSLIST, BossListBean.class, params, new com.android.volley.Response.Listener<BossListBean>() {
            @Override
            public void onResponse(BossListBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);


                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }
}
