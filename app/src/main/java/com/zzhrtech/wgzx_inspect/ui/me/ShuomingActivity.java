package com.zzhrtech.wgzx_inspect.ui.me;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.adapters.me.ShuomingAdapter;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.me.ShuomingBean;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShuomingActivity extends BaseActivity {

    private RecyclerView recy_shuoming;
    private ShuomingAdapter shuomingAdapter;
    List<ShuomingBean.Data> dataList;
    private String baseUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shuoming);

        toolbar.setNavigationIcon(R.mipmap.ic_close);


        recy_shuoming = (RecyclerView) findViewById(R.id.recy_shuoming);
        recy_shuoming.setLayoutManager(new LinearLayoutManager(this));
        recy_shuoming.setItemAnimator(new DefaultItemAnimator());

        shuomingAdapter = new ShuomingAdapter();
        recy_shuoming.setAdapter(shuomingAdapter);

        shuomingAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(ShuomingActivity.this,WebviewActivity.class);
                intent.putExtra("title",dataList.get(position).getTitle());
                intent.putExtra("url",baseUrl + "/id/" + dataList.get(position).getId());
                startActivity(intent);
            }
        });
//

        httpGetArea();
//        listAdapter = new JfmxListAdapter();
//        recy_jfmx.setAdapter(listAdapter);
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            ShuomingBean bean = (ShuomingBean) msg.obj;

            dataList = bean.getData();
            baseUrl = bean.getLink();
            for (ShuomingBean.Data data : dataList){
                shuomingAdapter.addItem(data);
            }

            return false;
        }
    });

    private void httpGetArea(){

        GsonRequest<ShuomingBean> gsonRequest = new GsonRequest<ShuomingBean>(com.android.volley.Request.Method.POST, HTTP_DETAIL, ShuomingBean.class, null, new com.android.volley.Response.Listener<ShuomingBean>() {
            @Override
            public void onResponse(ShuomingBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response;
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, reqTag);
    }
}
