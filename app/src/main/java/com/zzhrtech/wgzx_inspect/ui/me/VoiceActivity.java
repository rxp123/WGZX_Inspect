package com.zzhrtech.wgzx_inspect.ui.me;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.beans.me.VoiceBean;
import com.zzhrtech.wgzx_inspect.beans.task.BaseBean;
import com.zzhrtech.wgzx_inspect.beans.task.StatusBean;
import com.zzhrtech.wgzx_inspect.ui.task.TaskDetailActivity;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.HashMap;
import java.util.Map;

public class VoiceActivity extends BaseActivity {
    private CheckBox check_que,check_zhi;
    private Button btn_cun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        check_que = (CheckBox) findViewById(R.id.check_que);
        check_zhi = (CheckBox) findViewById(R.id.check_zhi);
        btn_cun = (Button) findViewById(R.id.btn_cun);


        httpGetvoice();

        btn_cun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                httpSetvoice();
            }
        });

//        Log.d("uid=======",getCache().getAsString(keyUid));
    }


    private void httpGetvoice(){
        Map<String,String> params = new HashMap<String, String>();
        params.put("uid",getCache().getAsString(keyUid));

        GsonRequest<VoiceBean> gsonRequest = new GsonRequest<VoiceBean>(com.android.volley.Request.Method.POST, Constans.HTTP_GETVOICE, VoiceBean.class, params, new com.android.volley.Response.Listener<VoiceBean>() {
            @Override
            public void onResponse(VoiceBean response) {

                if (response.isSuccess()){
                    Message message = new Message();
                    message.what = 0;
                    message.obj = response.getData();
                    mHandler.sendMessage(message);
                }else {
                    showToast(response.getMsg());
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private void httpSetvoice(){
        Map<String,String> params = new HashMap<String, String>();
        params.put("uid",getCache().getAsString(keyUid));
        if (check_que.isChecked()){
            params.put("quesheng","1");
        }else {
            params.put("quesheng","2");
        }

        if (check_zhi.isChecked()) {
            params.put("paisheng","1");
        }else {
            params.put("paisheng","2");
        }

        GsonRequest<BaseBean> gsonRequest = new GsonRequest<BaseBean>(com.android.volley.Request.Method.POST, Constans.HTTP_SETVOICE, BaseBean.class, params, new com.android.volley.Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

                if (response.isSuccess()){

                    showToast("设置完成");
                    finish();

                }else {
                    showToast(response.getMsg());
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what){
                case 0:

                    VoiceBean.Data data = (VoiceBean.Data) msg.obj;

                    if (data.getPaisheng().equals("1")){
                        check_zhi.setChecked(true);
                    }else {
                        check_zhi.setChecked(false);
                    }

                    if (data.getQuesheng().equals("1")){
                        check_que.setChecked(true);
                    }else {
                        check_que.setChecked(false);
                    }

                    break;
            }

            return false;
        }
    });

}
