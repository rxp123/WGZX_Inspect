package com.zzhrtech.wgzx_inspect.ui.contact;

import android.app.Activity;
import android.os.Bundle;

import com.zzhrtech.wgzx_inspect.BaseActivity;
import com.zzhrtech.wgzx_inspect.R;

public class ContactAllActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_all);

        toolbar.setNavigationIcon(R.mipmap.ic_close);

        getSupportFragmentManager().beginTransaction().add(R.id.contentView,ContactallFragment.newInstance()).commit();
    }
}
