package com.zzhrtech.wgzx_inspect.adapters.disease;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.disease.CateAllBean;
import com.zzhrtech.wgzx_inspect.beans.disease.DiseaseTypeBean;
import com.zzhrtech.wgzx_inspect.beans.disease.RoleListBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemDiseasetypeBinding;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by renxiangpeng on 16/7/5.
 */
public class DiseaseTypeAdapter extends RecyclerView.Adapter<DiseaseTypeAdapter.DiseaseHolder> implements View.OnClickListener {

    private List<CateAllBean.Data> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public DiseaseTypeAdapter(){
        beanList = new ArrayList<CateAllBean.Data>();
    }
    @Override
    public DiseaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_diseasetype, parent, false);

        itemView.setOnClickListener(this);
        return new DiseaseHolder(itemView);
    }


    @Override
    public void onBindViewHolder(DiseaseHolder holder, int position) {

//        holder.mBinding.ivType
        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class DiseaseHolder extends RecyclerView.ViewHolder {
        private ItemDiseasetypeBinding mBinding;

        public DiseaseHolder(View itemView) {
            super(itemView);
            mBinding = ItemDiseasetypeBinding.bind(itemView);
        }

        public void bind(@NonNull CateAllBean.Data user) {
            mBinding.setDisease(user);
        }
    }

    //添加列表
    public void addItem(CateAllBean.Data bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }

    @BindingAdapter({"android:src"})
    public static void setImageUrl(ImageView view, int url){
        view.setImageResource(url);
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}