package com.zzhrtech.wgzx_inspect.adapters.task;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.GsonRequest;
import com.squareup.picasso.Picasso;
import com.zzhrtech.WgzxApplication;
import com.zzhrtech.wgzx_inspect.Constans;
import com.zzhrtech.wgzx_inspect.MainActivity;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.disease.DiseaseTypeBean;
import com.zzhrtech.wgzx_inspect.beans.me.LoginBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.beans.task.UserInfoBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemDiseasetypeBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemTasklistBinding;
import com.zzhrtech.wgzx_inspect.utils.AppLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskListHolder> implements View.OnClickListener {

    private List<TaskListBean.Data> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public TaskListAdapter(){
        beanList = new ArrayList<TaskListBean.Data>();
    }
    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tasklist, parent, false);
        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TaskListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemTasklistBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemTasklistBinding.bind(itemView);
        }

        public void bind(@NonNull TaskListBean.Data user) {
            mBinding.setTask(user);
        }
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loagImage(final ImageView view,final String url){
        if (url != null) {
            if (!url.isEmpty()) {
                Picasso.with(view.getContext())
                        .load(url)
                        .placeholder(R.mipmap.ic_placeholde)
                        .error(R.mipmap.ic_placeholde)
                        .into(view);
            }
        }
    }


    @BindingAdapter(value={"bind:paiText","app:paiSource"},requireAll = false)
    public static void getpaiText(final TextView view, String text, final String source){

        Map<String,String> params = new HashMap<String, String>();

        params.put("uid",text);
        params.put("source",source);

        GsonRequest<UserInfoBean> gsonRequest = new GsonRequest<UserInfoBean>(com.android.volley.Request.Method.POST, Constans.HTTP_GETUSERINFO, UserInfoBean.class, params, new com.android.volley.Response.Listener<UserInfoBean>() {
            @Override
            public void onResponse(UserInfoBean response) {

                if (response.isSuccess()){
                    if (source.equals("5")){
                        view.setText("发布人：" + response.getData().getNickname() + "（市民）");
                    }else {
                        view.setText("发布人：" + response.getData().getNickname());
                    }
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppLogger.d(error + "");
            }
        });

        WgzxApplication.addRequest(gsonRequest, Constans.reqTag);

    }

    //添加列表
    public void addItem(TaskListBean.Data bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }
    //清空列表
    public void cleanList(){
        beanList.clear();
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}