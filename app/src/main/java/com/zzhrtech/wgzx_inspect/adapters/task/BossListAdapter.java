package com.zzhrtech.wgzx_inspect.adapters.task;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.task.BossListBean;
import com.zzhrtech.wgzx_inspect.beans.task.PhoneListBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemBosslistBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemPhonelistBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class BossListAdapter extends RecyclerView.Adapter<BossListAdapter.PhoneListHolder> implements View.OnClickListener {

    private List<BossListBean.Data> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public BossListAdapter(){
        beanList = new ArrayList<BossListBean.Data>();
    }
    @Override
    public PhoneListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bosslist, parent, false);

        itemView.setOnClickListener(this);
        return new PhoneListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(PhoneListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class PhoneListHolder extends RecyclerView.ViewHolder {
        private ItemBosslistBinding mBinding;

        public PhoneListHolder(View itemView) {
            super(itemView);
            mBinding = ItemBosslistBinding.bind(itemView);
        }

        public void bind(@NonNull BossListBean.Data user) {
            mBinding.setBoss(user);
        }
    }

    //添加列表
    public void addItem(BossListBean.Data bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }

    public String getBossId(int position){
        return beanList.get(position).getUid();
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}