package com.zzhrtech.wgzx_inspect.adapters.contact;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.ConversationBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemListConverBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemTasklistBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class ConverstionListAdapter extends RecyclerView.Adapter<ConverstionListAdapter.TaskListHolder> implements View.OnClickListener {

    private List<ConversationBean> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public ConverstionListAdapter(){
        beanList = new ArrayList<ConversationBean>();
    }
    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_conver, parent, false);

        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TaskListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemListConverBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemListConverBinding.bind(itemView);
        }

        public void bind(@NonNull ConversationBean user) {
            mBinding.setConver(user);
        }
    }
    //添加列表
    public void addItem(ConversationBean bean){
        beanList.add(0,bean);
        notifyDataSetChanged();
    }

    public void addlastMessage(String id,String message,String time,long unreader){
        for (ConversationBean iden : beanList){
            if (iden.getIdentifier().equals(id)){
                iden.setLastmessage(message);
                iden.setLastmessagetime(time);
                iden.setNumber(unreader);
            }
        }
        notifyDataSetChanged();
    }
    //清空列表
    public void cleanList(){
        beanList.clear();
    }
    public List<ConversationBean>  getBeanList(){
        return beanList;
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}