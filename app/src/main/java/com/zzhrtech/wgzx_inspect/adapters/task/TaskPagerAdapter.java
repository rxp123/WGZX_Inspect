package com.zzhrtech.wgzx_inspect.adapters.task;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by renxiangpeng on 16/7/15.
 */
public class TaskPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> mFragments;
    String[] mTitles;
    public TaskPagerAdapter(FragmentManager fm,ArrayList<Fragment> fragments,String[] titles) {
        super(fm);
        mFragments = fragments;
        mTitles = titles;
    }



    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


}