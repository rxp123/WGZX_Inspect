package com.zzhrtech.wgzx_inspect.adapters.task;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnCommCliskListener;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.task.PhoneListBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemPhonelistBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemTasklistBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class PhoneListAdapter extends RecyclerView.Adapter<PhoneListAdapter.PhoneListHolder> implements View.OnClickListener {

    private List<PhoneListBean.Data> beanList;
//    private List<EditText> personViews,phoneViews;
    private OnCommCliskListener mOnItemClickLitener;

    public PhoneListAdapter(){
        beanList = new ArrayList<PhoneListBean.Data>();
//        personViews = new ArrayList<EditText>();
//        phoneViews = new ArrayList<EditText>();
    }
    @Override
    public PhoneListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_phonelist, parent, false);

        itemView.setOnClickListener(this);
        return new PhoneListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(PhoneListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
//        holder.itemView.setTag(position);
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("pos",position);
        map.put("name",holder.mBinding.tvName);
        map.put("per",holder.mBinding.tvPerson);
        map.put("pho",holder.mBinding.tvPhone);
        holder.itemView.setTag(map);
//        personViews.add(holder.mBinding.tvPerson);
//        phoneViews.add(holder.mBinding.tvPhone);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class PhoneListHolder extends RecyclerView.ViewHolder {
        private ItemPhonelistBinding mBinding;

        public PhoneListHolder(View itemView) {
            super(itemView);
            mBinding = ItemPhonelistBinding.bind(itemView);
        }

        public void bind(@NonNull PhoneListBean.Data user) {
            mBinding.setPhone(user);
        }
    }

    //添加列表
    public void addItem(PhoneListBean.Data bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }

    public void setOnItemClickLitener(OnCommCliskListener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) ((Map) v.getTag()).get("pos"),(TextView) ((Map) v.getTag()).get("name"),(EditText) ((Map) v.getTag()).get("per"),(EditText) ((Map) v.getTag()).get("pho"));
        }
    }

}