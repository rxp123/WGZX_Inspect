package com.zzhrtech.wgzx_inspect.adapters.contact;

import android.content.Intent;
import android.databinding.BindingAdapter;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tencent.TIMElemType;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.ChatListBean;
import com.zzhrtech.wgzx_inspect.beans.ConversationBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemListChatBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemListConverBinding;
import com.zzhrtech.wgzx_inspect.ui.contact.ChatActivity;
import com.zzhrtech.wgzx_inspect.ui.contact.ImageViewActivity;
import com.zzhrtech.wgzx_inspect.ui.contact.VideoActivity;
import com.zzhrtech.wgzx_inspect.utils.FileUtil;
import com.zzhrtech.wgzx_inspect.utils.MediaUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.TaskListHolder> implements View.OnClickListener {

    private List<ChatListBean> beanList;
    private OnItemClickLitener mOnItemClickLitener;
    public ChatListAdapter() {
        beanList = new ArrayList<ChatListBean>();
    }

    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_chat, parent, false);

        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final TaskListHolder holder, final int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

        holder.mBinding.layoutMessagel.removeAllViews();
        holder.mBinding.layoutMessager.removeAllViews();

        if (beanList.get(position).getImage() != null) {
            if (!beanList.get(position).getImage().isEmpty()) {

                ImageView imageView = new ImageView(holder.mBinding.layoutMessagel.getContext());

                if (beanList.get(position).isSelf()) {
                    holder.mBinding.layoutMessager.addView(imageView);
                } else {
                    holder.mBinding.layoutMessagel.addView(imageView);
                }

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (beanList.get(position).getType() == TIMElemType.Image) {
                            Intent intent = new Intent(holder.itemView.getContext(), ImageViewActivity.class);
                            intent.putExtra("filename", beanList.get(position).getImageOrigin());
                            holder.itemView.getContext().startActivity(intent);
                        } else {
                            Intent intent = new Intent(holder.itemView.getContext(), VideoActivity.class);
                            intent.putExtra("path", beanList.get(position).getVideoPath());
                            holder.itemView.getContext().startActivity(intent);
                        }
                    }
                });



                if (beanList.get(position).getImage().startsWith("http")) {
                    Picasso.with(holder.mBinding.layoutMessagel.getContext())
                            .load(beanList.get(position).getImage())
                            .tag(ChatActivity.TAG)
                            .error(R.drawable.shape_tran)
                            .into(imageView);
                } else {

                    Picasso.with(holder.mBinding.layoutMessagel.getContext())
                            .load(new File(beanList.get(position).getImage()))
                            .tag(ChatActivity.TAG)
                            .resize(200, 300)
                            .error(R.drawable.shape_tran)
                            .into(imageView);
                }
            }
        }


        holder.mBinding.layoutLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AnimationDrawable frameAnimatio = (AnimationDrawable) holder.mBinding.ivVoicl.getBackground();
                try {
                    File tempAudio = FileUtil.getTempFile(FileUtil.FileType.AUDIO);
                    FileOutputStream fos = new FileOutputStream(tempAudio);
                    fos.write(beanList.get(position).getVoice());
                    fos.close();
                    FileInputStream fis = new FileInputStream(tempAudio);
                    MediaUtil.getInstance().play(fis);
                    frameAnimatio.start();
                    MediaUtil.getInstance().setEventListener(new MediaUtil.EventListener() {
                        @Override
                        public void onStop() {
                            frameAnimatio.stop();
                            frameAnimatio.selectDrawable(0);
                        }
                    });
                } catch (IOException e) {

                }
            }
        });

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (beanList.get(position).getType() == TIMElemType.Image) {
//                    Intent intent = new Intent(holder.itemView.getContext(), ImageViewActivity.class);
//                    intent.putExtra("filename", beanList.get(position).getImageOrigin());
//                    holder.itemView.getContext().startActivity(intent);
//                } else {
//                    Intent intent = new Intent(holder.itemView.getContext(), VideoActivity.class);
//                    intent.putExtra("path", beanList.get(position).getVideoPath());
//                    holder.itemView.getContext().startActivity(intent);
//                }
//
//            }
//        });

        holder.mBinding.layoutRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AnimationDrawable frameAnimatio = (AnimationDrawable) holder.mBinding.ivVoicr.getBackground();
                try {
                    File tempAudio = FileUtil.getTempFile(FileUtil.FileType.AUDIO);
                    FileOutputStream fos = new FileOutputStream(tempAudio);
                    fos.write(beanList.get(position).getVoice());
                    fos.close();
                    FileInputStream fis = new FileInputStream(tempAudio);
                    MediaUtil.getInstance().play(fis);
                    frameAnimatio.start();
                    MediaUtil.getInstance().setEventListener(new MediaUtil.EventListener() {
                        @Override
                        public void onStop() {
                            frameAnimatio.stop();
                            frameAnimatio.selectDrawable(0);
                        }
                    });
                } catch (IOException e) {

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemListChatBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemListChatBinding.bind(itemView);
        }

        public void bind(@NonNull ChatListBean user) {
            mBinding.setChat(user);
        }
    }


    @BindingAdapter({"bind:soundUrl"})
    public static void  soundImage(final ImageView view, int url) {
        if (url != 0) {
            view.setBackgroundResource(url);
        } else {
            view.setBackgroundResource(R.drawable.shape_tran);
        }
    }

    //添加列表 初始化
    public void initaddItem(ChatListBean bean) {
        beanList.add(0, bean);
        notifyDataSetChanged();
    }

    public void addItem(ChatListBean bean) {
        beanList.add(bean);
        notifyDataSetChanged();
    }

    //清空列表
    public ChatListBean getbean(int position) {
        return beanList.get(position);
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null) {
            mOnItemClickLitener.onItemClick(v, (int) v.getTag());
        }
    }
}