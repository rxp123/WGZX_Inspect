package com.zzhrtech.wgzx_inspect.adapters.contact;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.ConversationBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemListContBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemListConverBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class ContactallListAdapter extends RecyclerView.Adapter<ContactallListAdapter.TaskListHolder> implements View.OnClickListener {

    private List<ConversationBean> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public ContactallListAdapter(){
        beanList = new ArrayList<ConversationBean>();
    }
    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_cont, parent, false);

        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TaskListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemListContBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemListContBinding.bind(itemView);
        }

        public void bind(@NonNull ConversationBean user) {
            mBinding.setConver(user);
        }
    }

    //添加列表
    public void addItem(ConversationBean bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }
    //清空列表
    public void cleanList(){
        beanList.clear();
    }

    public String getIden(int position){
        return beanList.get(position).getIdentifier();
    }

    public String getNickname(int position){
        return beanList.get(position).getNickname();
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}