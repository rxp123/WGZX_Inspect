package com.zzhrtech.wgzx_inspect.adapters.task;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.task.TaskFilterBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskListBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemListQudaoBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemTasklistBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class TaskFilterAdapter extends RecyclerView.Adapter<TaskFilterAdapter.TaskListHolder> implements View.OnClickListener {

    private List<TaskFilterBean.Data> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public TaskFilterAdapter(){
        beanList = new ArrayList<TaskFilterBean.Data>();
    }
    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_qudao, parent, false);

        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TaskListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemListQudaoBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemListQudaoBinding.bind(itemView);
        }

        public void bind(@NonNull TaskFilterBean.Data user) {
            mBinding.setTask(user);
        }
    }


    //添加列表
    public void addItem(TaskFilterBean.Data bean){
        beanList.add(0,bean);
        notifyDataSetChanged();
    }
    //清空列表
    public void cleanList(){
        beanList.clear();
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}