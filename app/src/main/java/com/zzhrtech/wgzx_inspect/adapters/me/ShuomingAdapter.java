package com.zzhrtech.wgzx_inspect.adapters.me;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zzhrtech.wgzx_inspect.R;
import com.zzhrtech.wgzx_inspect.base.OnItemClickLitener;
import com.zzhrtech.wgzx_inspect.beans.me.ShuomingBean;
import com.zzhrtech.wgzx_inspect.beans.task.TaskFilterBean;
import com.zzhrtech.wgzx_inspect.databinding.ItemListQudaoBinding;
import com.zzhrtech.wgzx_inspect.databinding.ItemShuomingBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by renxiangpeng on 16/7/5.
 */
public class ShuomingAdapter extends RecyclerView.Adapter<ShuomingAdapter.TaskListHolder> implements View.OnClickListener {

    private List<ShuomingBean.Data> beanList;
    private OnItemClickLitener mOnItemClickLitener;

    public ShuomingAdapter(){
        beanList = new ArrayList<ShuomingBean.Data>();
    }
    @Override
    public TaskListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shuoming, parent, false);

        itemView.setOnClickListener(this);
        return new TaskListHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TaskListHolder holder, int position) {

        holder.bind(beanList.get(position));
        //为控件设置一个tag,点击事件用
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return beanList.size();
    }

    public static class TaskListHolder extends RecyclerView.ViewHolder {
        private ItemShuomingBinding mBinding;

        public TaskListHolder(View itemView) {
            super(itemView);
            mBinding = ItemShuomingBinding.bind(itemView);
        }

        public void bind(@NonNull ShuomingBean.Data user) {
            mBinding.setShuomi(user);
        }
    }


    //添加列表
    public void addItem(ShuomingBean.Data bean){
        beanList.add(bean);
        notifyDataSetChanged();
    }
    //清空列表
    public void cleanList(){
        beanList.clear();
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener)
    {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickLitener != null){
            mOnItemClickLitener.onItemClick(v,(int) v.getTag());
        }
    }
}