package com.zzhrtech.wgzx_inspect;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.tencent.TIMCallBack;
import com.tencent.TIMManager;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.qcloud.presentation.business.InitBusiness;
import com.tencent.qcloud.presentation.business.LoginBusiness;
import com.tencent.qcloud.presentation.event.FriendshipEvent;
import com.tencent.qcloud.presentation.event.MessageEvent;
import com.zzhrtech.wgzx_inspect.ui.contact.PushUtil;
import com.zzhrtech.wgzx_inspect.ui.me.LoginActivity;

/**
 * Created by renxiangpeng on 16/7/20.
 */
public class GuideActivity extends BaseActivity implements TIMCallBack {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_guide);

        XGPushManager.registerPush(getApplicationContext());

        //初始化
        InitBusiness.start(getApplicationContext());
        FriendshipEvent.getInstance().init();

        //好友关系
        if (getCache().getAsString(keyUid) == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }else {
            loginim();
        }

    }

    private void loginim(){
        //登陆
        LoginBusiness.loginIm(getCache().getAsString(keyUsername), getCache().getAsString(keySig), this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 3 & resultCode == RESULT_OK){
            loginim();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);// 必须要调用这句
    }

    @Override
    public void onError(int i, String s) {
        switch (i) {
            case 6208:
                new AlertDialog.Builder(GuideActivity.this)
                        .setMessage("你的账号已在其他终端登录,重新登录")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                loginim();
                            }
                        }).create().show();
                break;
        }
    }

    @Override
    public void onSuccess() {

        PushUtil.getInstance();
        Intent intent = new Intent(GuideActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
