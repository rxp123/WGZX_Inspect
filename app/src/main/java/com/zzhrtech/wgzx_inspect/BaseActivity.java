package com.zzhrtech.wgzx_inspect;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tencent.av.NetworkUtil;
import com.zzhrtech.wgzx_inspect.ui.disease.DiseaseUploadActivity;
import com.zzhrtech.wgzx_inspect.utils.ACache;
import com.zzhrtech.wgzx_inspect.utils.NetWorkUtils;

/**
 * Created by renxiangpeng on 16/7/4.
 */
public class BaseActivity extends AppCompatActivity implements Constans {

    protected Toolbar toolbar;
    ACache mcache;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//        if (!NetWorkUtils.isNetworkConnected(this)){

            if (!NetWorkUtils.isWifiConnected(this) && !NetWorkUtils.isMobileConnected(this)){
//            new MaterialDialog.Builder(this)
//                    .title("当前无网络")
//                    .content("退出当前界面")
//                    .negativeText("取消")
//                    .positiveText("确定")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                            finish();
//                            dialog.dismiss();
//                        }
//                    })
//                    .show();
            showToast("网络连接不通畅，请检查网络后重试");

        }
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initToolBar();
    }

    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            this.setSupportActionBar(this.toolbar);
            this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    BaseActivity.this.finish();
                }
            });
        }
    }

    protected void setTitle(String title) {
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    //根据id弹出toast
    public void showToast(int resId) {
        showToast(getString(resId));
    }
    //根据文字弹出toast
    public void showToast(String msg) {
        if (!isFinishing()) {

            Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
//            Snackbar.make(view,msg,Snackbar.LENGTH_SHORT).show();
        }
    }

    public void setTitle(@StringRes int resId) {
        this.setTitle(this.getString(resId));
    }


    public ACache getCache(){
        if (mcache == null) {
            mcache = ACache.get(this);
        }
        return mcache;
    }

}
