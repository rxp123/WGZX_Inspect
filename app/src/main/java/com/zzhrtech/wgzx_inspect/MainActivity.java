package com.zzhrtech.wgzx_inspect;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.loveplusplus.update.UpdateChecker;
import com.tencent.TIMManager;
import com.tencent.TIMUserStatusListener;
import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushManager;
import com.zzhrtech.wgzx_inspect.ui.chat.ConversationFragment;
import com.zzhrtech.wgzx_inspect.ui.contact.ContactFragment;
import com.zzhrtech.wgzx_inspect.ui.disease.DiseaseFragment;
import com.zzhrtech.wgzx_inspect.entity.TabEntity;
import com.zzhrtech.wgzx_inspect.ui.me.MeFragment;
import com.zzhrtech.wgzx_inspect.ui.task.TaskFragment;
import com.zzhrtech.wgzx_inspect.utils.DensityUtil;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private String[] mTitles = {"我的任务", "填报问题", "消息", "更多"};
    private int[] mIconUnselectIds = {
            R.mipmap.tab_home_unselect, R.mipmap.tab_speech_unselect,
            R.mipmap.tab_contact_unselect, R.mipmap.tab_more_unselect};
    private int[] mIconSelectIds = {
            R.mipmap.tab_home_select, R.mipmap.tab_speech_select,
            R.mipmap.tab_contact_select, R.mipmap.tab_more_select};
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<CustomTabEntity>();
    private ArrayList<Fragment> mFragments = new ArrayList<Fragment>();

    private ViewPager vp_main;
    private CommonTabLayout tab_main;
    private MyPagerAdapter myPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        UpdateChecker.checkForDialog(this,false);

        XGPushManager.registerPush(this, getCache().getAsString(keyUsername),
                new XGIOperateCallback() {
                    @Override
                    public void onSuccess(Object data, int flag) {
                        Log.d("TPush", "注册成功，设备token为：" + data);
                    }

                    @Override
                    public void onFail(Object data, int errCode, String msg) {
                        Log.d("TPush", "注册失败，错误码：" + errCode + ",错误信息：" + msg);
                    }
                });

        TIMManager.getInstance().setUserStatusListener(new TIMUserStatusListener() {
            @Override
            public void onForceOffline() {
                //互踢
            }

            @Override
            public void onUserSigExpired() {
                //过期
            }
        });


    }

    private void initView(){

        vp_main = (ViewPager) findViewById(R.id.vp_main);
        tab_main = (CommonTabLayout) findViewById(R.id.tab_main);

        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }

        mFragments.add(TaskFragment.newInstance());
        mFragments.add(DiseaseFragment.newInstance("",""));
//        mFragments.add(ConversationFragment.newInstance("",""));
        mFragments.add(new ConversationFragment());
        mFragments.add(MeFragment.newInstance());


        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        vp_main.setAdapter(myPagerAdapter);

        tab_main.setTabData(mTabEntities);

        tab_main.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vp_main.setCurrentItem(position);

            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        vp_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tab_main.setCurrentTab(position);

//                if (position == 2){
//                    toolbar.setVisibility(View.GONE);
//                }else {
//                    toolbar.setVisibility(View.VISIBLE);
                    setTitle(mTitles[position]);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        vp_main.setOffscreenPageLimit(3);
        setTitle("我的任务");
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 & resultCode == RESULT_OK){
            myPagerAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {


        new MaterialDialog.Builder(MainActivity.this)
                .content("是否退出？")
                .negativeText("取消")
                .positiveText("确定")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .show();
    }

    public void setMsgUnread(long unread){

        if (unread == 0){
            tab_main.hideMsg(2);
        }else {
            tab_main.showMsg(2,(int) unread);
        }

        //两位数
//        mTabLayout_2.showMsg(0, 55);
//        mTabLayout_2.setMsgMargin(0, -5, 5);
    }

    //    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);// 必须要调用这句
//    }

    //
}
