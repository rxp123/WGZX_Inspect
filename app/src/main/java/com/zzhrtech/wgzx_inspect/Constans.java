package com.zzhrtech.wgzx_inspect;

/**
 * Created by renxiangpeng on 16/7/13.
 */
public interface Constans {
    public static final String reqTag = "requestTag";
    public static final String keyUid = "uid";
    public static final String keyDepid = "depid";
    public static final String keyHead = "head";
    public static final String keyState = "state";
    public static final String keyUsername = "username";
    public static final String keyNickname = "nickname";
    public static final String keyPassword = "password";
    public static final String keyPid = "pid";
    public static final String keySig = "sig";
    public static final String keyIsUserboss = "isUserboss";
    public static final String keyIsboss = "isboss";



    public static final String HTTP_SERVER = "http://ccwg.zzhrtech.com/index.php/Api/"; //主地址

    public static final String HTTP_GETDepartList = HTTP_SERVER + "Department/getList";//获取部门

    public static final String HTTP_CATELIST = HTTP_SERVER + "Category/getListByDepId";//获取病害类型
    public static final String HTTP_CATELISTALL = HTTP_SERVER + "Category/getList";//获取病害类型
    public static final String HTTP_CATEINFO = HTTP_SERVER + "Category/getCategoryInfo";//获取病害种类
    public static final String HTTP_CATECONTENT = HTTP_SERVER + "Category/getCisByCiId";//获取病害详情


    public static final String HTTP_TASKADD = HTTP_SERVER + "Task/add";//上传病害
    public static final String HTTP_TASKLIST = HTTP_SERVER + "Task/getListByStatus";//任务列表
    public static final String HTTP_TASKDETAIL = HTTP_SERVER + "Task/getListById";//任务详情
    public static final String HTTP_TASKTURNSEND = HTTP_SERVER + "Task/setTaskStatus";//任务处理
    public static final String HTTP_TASKHUIFU = HTTP_SERVER + "Task/huiFu";//任务恢复

    public static final String HTTP_LOGIN = HTTP_SERVER + "User/login";//登录
    public static final String HTTP_CHPASS = HTTP_SERVER + "User/chPass";//修改密码
    public static final String HTTP_AREALIST = HTTP_SERVER + "Area/getListByDepId";//区域列表
    public static final String HTTP_AREALISTX = HTTP_SERVER + "Area/getListByDepIdx";//全部区域列表

    public static final String HTTP_BULDERLIST = HTTP_SERVER + "Builder/getList";//施工单位
    public static final String HTTP_GETDEPART = HTTP_SERVER + "Department/getListById";//获取部门信息
    public static final String HTTP_GETALLCOUNT = HTTP_SERVER + "Count/getCountExplodeByDep";//获取部门统计数据
    public static final String HTTP_GETITEMCOUNT = HTTP_SERVER + "Count/getCountByStatus";//获取统计数据
    public static final String HTTP_TASKCANCLE = HTTP_SERVER + "Reason/getList";//取消任务原因
    public static final String HTTP_TASKZHUAN = HTTP_SERVER + "Task/zhuanPai";//任务转派
    public static final String HTTP_TASKFEN = HTTP_SERVER + "Task/fenPai";//任务

    public static final String HTTP_TASKSHEN = HTTP_SERVER + "Task/shenPi";//任务审批
    public static final String HTTP_TASKLOG = HTTP_SERVER + "Task/getTaskLogById";//任务流程
    public static final String HTTP_ABOUT = HTTP_SERVER + "About/index";//根据用户名获取信息
    public static final String HTTP_SUGGEST = HTTP_SERVER + "Suggestion/add";//意见反馈
    public static final String HTTP_MAPPOINT = HTTP_SERVER + "Map/getList";//地图坐标
    public static final String HTTP_MAPVIDEO = HTTP_SERVER + "Map/getVideo";//地图视频
    public static final String HTTP_UPPI = HTTP_SERVER + "Task/upPi";//上层审批
    public static final String HTTP_BOSSLIST = HTTP_SERVER + "User/getBossList";//上级列表
    public static final String HTTP_ZHUANDEP = HTTP_SERVER + "Department/zhuanGetList";//转派部门列表
    public static final String HTTP_TASKDELET = HTTP_SERVER + "Task/delete";//任务删除
    public static final String HTTP_CANPAI = HTTP_SERVER + "Task/canPai";//是否可指派
    public static final String HTTP_CANZHUAN = HTTP_SERVER + "Task/canZhuan";//是否可转派
    public static final String HTTP_CANFEN = HTTP_SERVER + "Task/canFen";//是否可分派


    public static final String HTTP_PAIID = HTTP_SERVER + "User/getListByDepId";//获得部门人员列表
    public static final String HTTP_ZHIPAI = HTTP_SERVER + "Task/zhiPai";//指派任务
    public static final String HTTP_GETUSERINFO = HTTP_SERVER + "User/getInfoByUid";//根据uid获得用户信息

    public static final String HTTP_UNITLIST = HTTP_SERVER + "Unit/getList";//面积列表
    public static final String HTTP_DETAIL = HTTP_SERVER + "Detail/getList";//使用说明

    public static final String HTTP_GETVOICE = HTTP_SERVER + "User/getVoice";//获取声音
    public static final String HTTP_SETVOICE = HTTP_SERVER + "User/setVoice";//设置声音


}
