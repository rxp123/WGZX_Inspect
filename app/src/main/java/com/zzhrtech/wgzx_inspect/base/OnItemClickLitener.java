package com.zzhrtech.wgzx_inspect.base;

import android.view.View;

/**
 * Created by renxiangpeng on 16/7/5.
 */
public interface OnItemClickLitener
{
    void onItemClick(View view, int position);
}
