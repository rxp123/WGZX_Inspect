package com.zzhrtech.wgzx_inspect.base;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by renxiangpeng on 16/10/18.
 */
public interface OnCommCliskListener
{
    void onItemClick(View view, int position, TextView name, EditText person, EditText phone);
}
