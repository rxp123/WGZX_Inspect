package com.zzhrtech.wgzx_inspect.beans.me;

import java.util.List;

/**
 * Created by renxiangpeng on 16/9/1.
 */
public class MapVideoPointBean {
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        String video;
        List<String> point;


        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public List<String> getPoint() {
            return point;
        }

        public void setPoint(List<String> point) {
            this.point = point;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
