package com.zzhrtech.wgzx_inspect.beans.task;

/**
 * Created by renxiangpeng on 16/7/15.
 */
public class BaseBean {
    String code;
    String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
