package com.zzhrtech.wgzx_inspect.beans.me;

import java.util.List;

/**
 * Created by renxiangpeng on 16/9/1.
 */
public class MapPointBean {
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        String status;
        List<String> point;
        String cat_name;
        String ci_name;
        String cis_name;

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCi_name() {
            return ci_name;
        }

        public void setCi_name(String ci_name) {
            this.ci_name = ci_name;
        }

        public String getCis_name() {
            return cis_name;
        }

        public void setCis_name(String cis_name) {
            this.cis_name = cis_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getPoint() {
            return point;
        }

        public void setPoint(List<String> point) {
            this.point = point;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
