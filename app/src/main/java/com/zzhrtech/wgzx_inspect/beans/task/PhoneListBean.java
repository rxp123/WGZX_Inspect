package com.zzhrtech.wgzx_inspect.beans.task;

import java.util.List;

/**
 * Created by renxiangpeng on 16/7/12.
 */
public class PhoneListBean {
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        String b_id;
        String b_name;
        String b_phone;
        String b_person;

        public String getB_id() {
            return b_id;
        }

        public void setB_id(String b_id) {
            this.b_id = b_id;
        }

        public String getB_name() {
            return b_name;
        }

        public void setB_name(String b_name) {
            this.b_name = b_name;
        }

        public String getB_phone() {
            return b_phone;
        }

        public void setB_phone(String b_phone) {
            this.b_phone = b_phone;
        }

        public String getB_person() {
            return b_person;
        }

        public void setB_person(String b_person) {
            this.b_person = b_person;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
