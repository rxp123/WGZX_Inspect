package com.zzhrtech.wgzx_inspect.beans.task;

import java.util.List;

/**
 * Created by renxiangpeng on 16/12/28.
 */

public class UserInfoBean {
    String code;
    String msg;
    Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{

        String nickname;
        String isboss;

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getIsboss() {
            return isboss;
        }

        public void setIsboss(String isboss) {
            this.isboss = isboss;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
