package com.zzhrtech.wgzx_inspect.beans.task;

/**
 * Created by renxiangpeng on 2017/1/11.
 */

public class FkArceBean {
    String task_id;
    String unit_id;
    String cat_id;
    String ci_id;
    String cis_id;
    String val;


    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCi_id() {
        return ci_id;
    }

    public void setCi_id(String ci_id) {
        this.ci_id = ci_id;
    }

    public String getCis_id() {
        return cis_id;
    }

    public void setCis_id(String cis_id) {
        this.cis_id = cis_id;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
