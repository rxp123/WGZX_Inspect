package com.zzhrtech.wgzx_inspect.beans.disease;

/**
 * Created by renxiangpeng on 16/7/27.
 */
public class DepartInfoBean {
    String code;
    String msg;
    Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        String dep_id;
        String dep_name;

        public String getDep_id() {
            return dep_id;
        }

        public void setDep_id(String dep_id) {
            this.dep_id = dep_id;
        }

        public String getDep_name() {
            return dep_name;
        }

        public void setDep_name(String dep_name) {
            this.dep_name = dep_name;
        }
    }

    public boolean isSuccess(){

        return code.equals("0");

    }
}
