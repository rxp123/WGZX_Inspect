package com.zzhrtech.wgzx_inspect.beans.me;

import java.util.List;

/**
 * Created by renxiangpeng on 16/7/20.
 */
public class LoginBean {
    String code;
    String msg;
    Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        String uid;
        String dep_id;
        String head;
        String username;
        String nickname;
        String viewhead;
        String state;
        String pid;
        String sig;
        String isboss;

        public String getIsboss() {
            return isboss;
        }

        public void setIsboss(String isboss) {
            this.isboss = isboss;
        }

        public String getSig() {
            return sig;
        }

        public void setSig(String sig) {
            this.sig = sig;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getPid() {
            return pid;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public void setDep_id(String dep_id) {
            this.dep_id = dep_id;
        }

        public String getDep_id() {
            return dep_id;
        }

        public String getHead() {
            return head;
        }

        public void setHead(String head) {
            this.head = head;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getViewhead() {
            return viewhead;
        }

        public void setViewhead(String viewhead) {
            this.viewhead = viewhead;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
