package com.zzhrtech.wgzx_inspect.beans.task;

import android.databinding.BaseObservable;

import java.util.List;

/**
 * Created by renxiangpeng on 16/7/8.
 */
public class TaskListBean{
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        String task_id;
        String task_address;
        String task_date;
        String role_name;
        String cat_name;
        String ci_name;
        String oneImgTh;
        String a_name;
        String source;
        String uid;
        String isboss;
//        String stringSource;
        String zhong;

        String status;

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status.equals("0") ? "待确认" : status.equals("1") ? "已确认"
                    : status.equals("2") ? "已取消" : status.equals("4") ? "已派修"
                    : status.equals("5") ? "已反馈" : status.equals("6") ? "待审批"
                    : status.equals("7") ? "已审批" : "其他";
        }

        public String getIsboss() {
            return isboss;
        }

        public void setIsboss(String isboss) {
            this.isboss = isboss;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getUid() {
            return uid;
        }

        public String getA_name() {
            return a_name;
        }

        public void setA_name(String a_name) {
            this.a_name = a_name;
        }

        public String getSource() {
//            return null;
            return source;
        }

//        public void setStringSource(String stringSource) {
//            this.stringSource = stringSource;
//        }
//
//        public String getZhong() {
//            return zhong().equals("2") ? "急" : source.equals("3") ? "重" : source.equals("4") ? "急、重" : null;
//        }


        public String getZhong() {
            return zhong.equals("2") ? "急" : zhong.equals("3") ? "重" : zhong.equals("4") ? "急、重" : null;
        }

        public void setZhong(String zhong) {
            this.zhong = zhong;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getTask_id() {
            return task_id;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getTask_address() {
            return task_address;
        }

        public void setTask_address(String task_address) {
            this.task_address = task_address;
        }

        public String getTask_date() {
            return task_date;
        }

        public void setTask_date(String task_date) {
            this.task_date = task_date;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCi_name() {
            return ci_name;
        }

        public void setCi_name(String ci_name) {
            this.ci_name = ci_name;
        }

        public String getOneImgTh() {
            return oneImgTh;
        }

        public void setOneImgTh(String oneImgTh) {
            this.oneImgTh = oneImgTh;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
