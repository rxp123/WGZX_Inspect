package com.zzhrtech.wgzx_inspect.beans.task;

import java.util.List;

/**
 * Created by renxiangpeng on 16/8/26.
 */
public class TaskReasonBean {
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        String r_id;
        String r_name;

        public String getR_id() {
            return r_id;
        }

        public void setR_id(String r_id) {
            this.r_id = r_id;
        }

        public String getR_name() {
            return r_name;
        }

        public void setR_name(String r_name) {
            this.r_name = r_name;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
