package com.zzhrtech.wgzx_inspect.beans.disease;

import android.databinding.Bindable;
import android.databinding.Observable;

import com.zzhrtech.wgzx_inspect.R;

import java.util.List;

/**
 * Created by renxiangpeng on 16/7/26.
 */
public class CateAllBean {
    String code;
    String msg;
    List<Data> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static  class Data{
        String cat_id;
        String cat_name;
        String dep_id;
        int imgUrl;

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getDep_id() {
            return dep_id;
        }

        public void setDep_id(String dep_id) {
            this.dep_id = dep_id;
        }

        public int getImgUrl() {
            return cat_name.equals("排水") ? R.mipmap.ic_paishui : cat_name.equals("桥梁") ? R.mipmap.ic_qiaoliang :
                    cat_name.equals("道路") ? R.mipmap.ic_daolu : cat_name.equals("静态停车") ? R.mipmap.ic_tingche :
                            cat_name.equals("防汛积水点") ? R.mipmap.ic_fangxun : R.mipmap.ic_tongy;
//            return imgUrl;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
