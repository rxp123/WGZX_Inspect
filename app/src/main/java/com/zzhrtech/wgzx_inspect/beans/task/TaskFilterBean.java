package com.zzhrtech.wgzx_inspect.beans.task;

import java.util.List;

/**
 * Created by renxiangpeng on 16/8/28.
 */
public class TaskFilterBean {

    String msg;
    String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        String tl_id;
        String status;
        String tl_date;
        String dep_name;
        String nickname;
        String detail;
        String reason;

        public String getTl_id() {
            return tl_id;
        }

        public void setTl_id(String tl_id) {
            this.tl_id = tl_id;
        }

        public String getStatus() {
            return status.equals("0") ? "待确认" : status.equals("1") ? "已确认"
                    : status.equals("2") ? "已取消" : status.equals("4") ? "已派修"
                    : status.equals("5") ? "已反馈" : status.equals("6") ? "待审批"
                    : status.equals("7") ? "已审批" : "其他";
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTl_date() {
            return tl_date;
        }

        public void setTl_date(String tl_date) {
            this.tl_date = tl_date;
        }

        public String getDep_name() {
            return dep_name;
        }

        public void setDep_name(String dep_name) {
            this.dep_name = dep_name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
