package com.zzhrtech.wgzx_inspect.beans;

import com.tencent.TIMConversation;

/**
 * Created by renxiangpeng on 16/9/5.
 */
public class ConversationBean {
    String identifier;
    String nickname;
    String lastmessage;
    long number;
    String lastmessagetime;
    String head;
    TIMConversation conversation;

    public void setConversation(TIMConversation conversation) {
        this.conversation = conversation;
    }

    public TIMConversation getConversation() {
        return conversation;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getHead() {
        return head;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getNickname() {
        return nickname.equals("") || nickname == null ? identifier : nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }


    public void setNumber(long number) {
        this.number = number;
    }

    public long getNumber() {
        return number;
    }

    public String getLastmessagetime() {
        return lastmessagetime;
    }

    public void setLastmessagetime(String lastmessagetime) {
        this.lastmessagetime = lastmessagetime;
    }
}
