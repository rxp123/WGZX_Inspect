package com.zzhrtech.wgzx_inspect.beans.disease;

/**
 * Created by renxiangpeng on 16/7/5.
 */
public class DiseaseTypeBean {
    String id;
    String name;
    String imageUrl;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
