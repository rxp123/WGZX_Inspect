package com.zzhrtech.wgzx_inspect.beans;

import android.text.SpannableStringBuilder;

import com.tencent.TIMElemType;
import com.zzhrtech.wgzx_inspect.R;

/**
 * Created by renxiangpeng on 16/9/7.
 */
public class ChatListBean {
    String head;
    SpannableStringBuilder text;
    boolean isSelf;
    String time;
    String image;
    //大图
    String imageOrigin;
    int soundimg;
    TIMElemType type;
    String videoPath;
    byte[] voice;

    String thumbUuid;

    public String getThumbUuid() {
        return thumbUuid;
    }

    public void setThumbUuid(String thumbUuid) {
        this.thumbUuid = thumbUuid;
    }

    public void setVoice(byte[] voice) {
        this.voice = voice;
    }

    public byte[] getVoice() {
        return voice;
    }

    public void setImageOrigin(String imageOrigin) {
        this.imageOrigin = imageOrigin;
    }

    public String getImageOrigin() {
        return imageOrigin;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public void setType(TIMElemType type) {
        this.type = type;
    }

    public TIMElemType getType() {
        return type;
    }

    public void setSoundimg(int soundimg) {
        this.soundimg = soundimg;
    }

    public int getSoundimg() {
        if (soundimg == 1){
            if (isSelf()){
                return R.drawable.right_voice;
            }else {
                return R.drawable.left_voice;
            }
        }else {
            return 0;
        }
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setText(SpannableStringBuilder text) {
        this.text = text;
    }

    public SpannableStringBuilder getText() {
        return text;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }
}
