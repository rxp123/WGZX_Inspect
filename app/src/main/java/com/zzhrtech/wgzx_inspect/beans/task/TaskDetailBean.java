package com.zzhrtech.wgzx_inspect.beans.task;


/**
 * Created by renxiangpeng on 16/7/15.
 */
public class TaskDetailBean {

    String code;
    String msg;
    Data data;
    Area area;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        String task_id;
        String task_address;
        String dep_id;
        String dep_name;
        String cat_name;
        String ci_name;
        String cis_name;
        String task_point;
        String oneImgTh;
        String oneImg;
        String twoImg;
        String twoImgTh;
        String threeImg;
        String threeImgTh;
        String xiuOneImg;
        String xiuOneImgTh;
        String xiuTwoImg;
        String xiuTwoImgTh;
        String xiuThreeImg;
        String xiuThreeImgTh;
        String task_date;
        String task_name;
        String task_phone;
        String pi;
        String task_detail;
        String video;
        String uppi;
        String b_name;
        String b_phone;
        String b_person;
        Area area;
        String status;
        String fanneirong;
        String quOneImg;
        String quTwoImg;
        String quThreeImg;

        public String getQuOneImg() {
            return quOneImg;
        }

        public void setQuOneImg(String quOneImg) {
            this.quOneImg = quOneImg;
        }

        public String getQuTwoImg() {
            return quTwoImg;
        }

        public void setQuTwoImg(String quTwoImg) {
            this.quTwoImg = quTwoImg;
        }

        public String getQuThreeImg() {
            return quThreeImg;
        }

        public void setQuThreeImg(String quThreeImg) {
            this.quThreeImg = quThreeImg;
        }

        public String getFanneirong() {
            return fanneirong;
        }

        public void setFanneirong(String fanneirong) {
            this.fanneirong = fanneirong;
        }

        public String getXiuOneImg() {
            return xiuOneImg;
        }

        public void setXiuOneImg(String xiuOneImg) {
            this.xiuOneImg = xiuOneImg;
        }

        public String getXiuOneImgTh() {
            return xiuOneImgTh;
        }

        public void setXiuOneImgTh(String xiuOneImgTh) {
            this.xiuOneImgTh = xiuOneImgTh;
        }

        public String getXiuTwoImg() {
            return xiuTwoImg;
        }

        public void setXiuTwoImg(String xiuTwoImg) {
            this.xiuTwoImg = xiuTwoImg;
        }

        public String getXiuTwoImgTh() {
            return xiuTwoImgTh;
        }

        public void setXiuTwoImgTh(String xiuTwoImgTh) {
            this.xiuTwoImgTh = xiuTwoImgTh;
        }

        public String getXiuThreeImg() {
            return xiuThreeImg;
        }

        public void setXiuThreeImg(String xiuThreeImg) {
            this.xiuThreeImg = xiuThreeImg;
        }

        public String getXiuThreeImgTh() {
            return xiuThreeImgTh;
        }

        public void setXiuThreeImgTh(String xiuThreeImgTh) {
            this.xiuThreeImgTh = xiuThreeImgTh;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public String getB_name() {
            return b_name;
        }

        public void setB_name(String b_name) {
            this.b_name = b_name;
        }

        public String getB_phone() {
            return b_phone;
        }

        public void setB_phone(String b_phone) {
            this.b_phone = b_phone;
        }

        public String getB_person() {
            return b_person;
        }

        public void setB_person(String b_person) {
            this.b_person = b_person;
        }

        public void setUppi(String uppi) {
            this.uppi = uppi;
        }

        public String getUppi() {
            return uppi;
        }

        public String getCis_name() {
            return cis_name;
        }

        public void setCis_name(String cis_name) {
            this.cis_name = cis_name;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getVideo() {
            return video;
        }

        public void setPi(String pi) {
            this.pi = pi;
        }

        public String getPi() {
            return pi;
        }

        public void setArea(Area area) {
            this.area = area;
        }

        public Area getArea() {
            return area;
        }

        public String getDep_id() {
            return dep_id;
        }

        public void setDep_id(String dep_id) {
            this.dep_id = dep_id;
        }

        public String getDep_name() {
            return dep_name;
        }

        public void setDep_name(String dep_name) {
            this.dep_name = dep_name;
        }

        public String getTask_id() {
            return task_id;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getTask_address() {
            return task_address;
        }

        public void setTask_address(String task_address) {
            this.task_address = task_address;
        }


        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCi_name() {
            return ci_name;
        }

        public void setCi_name(String ci_name) {
            this.ci_name = ci_name;
        }

        public String getTask_point() {
            return task_point;
        }

        public void setTask_point(String task_point) {
            this.task_point = task_point;
        }

        public String getOneImgTh() {
            return oneImgTh;
        }

        public void setOneImgTh(String oneImgTh) {
            this.oneImgTh = oneImgTh;
        }

        public String getOneImg() {
            return oneImg;
        }

        public void setOneImg(String oneImg) {
            this.oneImg = oneImg;
        }

        public String getTwoImg() {
            return twoImg;
        }

        public void setTwoImg(String twoImg) {
            this.twoImg = twoImg;
        }

        public String getTwoImgTh() {
            return twoImgTh;
        }

        public void setTwoImgTh(String twoImgTh) {
            this.twoImgTh = twoImgTh;
        }

        public String getThreeImg() {
            return threeImg;
        }

        public void setThreeImg(String threeImg) {
            this.threeImg = threeImg;
        }

        public String getThreeImgTh() {
            return threeImgTh;
        }

        public void setThreeImgTh(String threeImgTh) {
            this.threeImgTh = threeImgTh;
        }

        public String getTask_date() {
            return task_date;
        }

        public void setTask_date(String task_date) {
            this.task_date = task_date;
        }

        public String getTask_name() {
            return task_name;
        }

        public void setTask_name(String task_name) {
            this.task_name = task_name;
        }

        public String getTask_phone() {
            return task_phone;
        }

        public void setTask_phone(String task_phone) {
            this.task_phone = task_phone;
        }

        public String getTask_detail() {
            return task_detail;
        }

        public void setTask_detail(String task_detail) {
            this.task_detail = task_detail;
        }
    }
    public static class Area{
        String a_id;
        String a_name;

        public String getA_id() {
            return a_id;
        }

        public void setA_id(String a_id) {
            this.a_id = a_id;
        }

        public String getA_name() {
            return a_name;
        }

        public void setA_name(String a_name) {
            this.a_name = a_name;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }
}
