package com.zzhrtech.wgzx_inspect.beans.task;

import java.util.List;

/**
 * Created by renxiangpeng on 16/9/20.
 */
public class BossListBean {
    String code;
    String msg;
    List<Data> data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        String uid;
        String nickname;
        String viewhead;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }


        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getViewhead() {
            return viewhead;
        }

        public void setViewhead(String viewhead) {
            this.viewhead = viewhead;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }

}
