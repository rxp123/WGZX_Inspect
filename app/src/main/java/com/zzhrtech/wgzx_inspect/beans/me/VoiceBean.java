package com.zzhrtech.wgzx_inspect.beans.me;

/**
 * Created by renxiangpeng on 2017/3/22.
 */

public class VoiceBean {
    String code;
    String msg;
    Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        String paisheng;
        String quesheng;

        public String getPaisheng() {
            return paisheng;
        }

        public void setPaisheng(String paisheng) {
            this.paisheng = paisheng;
        }

        public String getQuesheng() {
            return quesheng;
        }

        public void setQuesheng(String quesheng) {
            this.quesheng = quesheng;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }

}
