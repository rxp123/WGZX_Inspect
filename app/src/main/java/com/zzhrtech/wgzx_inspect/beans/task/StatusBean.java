package com.zzhrtech.wgzx_inspect.beans.task;

/**
 * Created by renxiangpeng on 2017/3/22.
 */

public class StatusBean {
    String code;
    String msg;
    Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public boolean isSuccess(){
        return code.equals("0");
    }

}
